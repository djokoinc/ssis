﻿using ALIT.Model;
using ALITools.ALIT.PaW.Plugins.Model;
using Microsoft.Xrm.Sdk;
using System;

namespace ALITools.ALIT.PaW.Plugins.Helper
{
    public static class CrmConfigurationManager
    {
        /// <summary>
        /// Gets the CRM configuration.
        /// </summary>
        /// <param name="adminService">The admin service.</param>
        /// <param name="initiatingUserId">The initiating user identifier.</param>
        /// <returns></returns>
        public static CrmConfiguration GetConfiguration(IOrganizationService adminService, Guid initiatingUserId)
        {
            // Retrieve PAW URL
            var pawUrl = CrmHelper.GetConfiguration(
                adminService,
                "PW.Url",
                mcs_configuration.Fields.mcs_value);

            // Retrieve PAW administrator username
            var pawAdminUser = CrmHelper.GetConfiguration(
                adminService,
                "PW.AdminUserName",
                mcs_configuration.Fields.mcs_securedvalue);

            // Retrieve PAW administrator password
            var pawAdminPassword = CrmHelper.GetConfiguration(
                adminService,
                "PW.AdminPassword",
                mcs_configuration.Fields.mcs_securedvalue);

            // Retrieve Plug & Work Device ID
            var pawDeviceId = CrmHelper.GetConfiguration(
                adminService,
                "PW.DeviceId",
                mcs_configuration.Fields.mcs_securedvalue);

            // Retrieve initiating user email
            var initiatingUserEmail = CrmHelper.GetInitiatingUserEmail(
                adminService,
                initiatingUserId);

            // Initialize a new instance of the CRM configuration object
            var crmConfiguration = new CrmConfiguration(pawUrl, pawAdminUser, pawAdminPassword, pawDeviceId, initiatingUserEmail);
            return crmConfiguration;
        }
    }
}
