﻿using ALIT.Model;
using ALITools.ALIT.PaW.Plugins.Model;
using Microsoft.Xrm.Sdk;

namespace ALITools.ALIT.PaW.Plugins.Helper
{
    public class SpaceConfigurationManager
    {
        public PawSpace Space { get; private set; }
        public string TemplateId { get; private set; }

        public SpaceConfigurationManager(
            IOrganizationService adminService,
            string entityLogicalName,
            string entityId,
            EntityReference subsidiaryId,
            string title,
            string email)
        {
            // Initialize space
            Space = new PawSpace()
            {
                Title = title
            };

            if (string.IsNullOrEmpty(email))
            {
                email = null;
            }

            // Assign entity ID for external key purpose
            Space.Superfields = new PawSuperfield()
            {
                EntityId = entityId.ToLowerInvariant(),
                Email = email
            };

            // Fetch Plug & Work tags configuration
            Space.Tags = CrmHelper.GetTags(adminService, entityLogicalName, subsidiaryId);

            // Fetch Plug & Work security configuration
            Space.Readers = CrmHelper.GetAssociatedGroups(
                adminService, entityLogicalName, subsidiaryId, mcs_configurationespacepaw_groupe_lecteurs.EntityLogicalName);
            Space.Writers = CrmHelper.GetAssociatedGroups(
                adminService, entityLogicalName, subsidiaryId, mcs_configurationespacepaw_groupe_contribut.EntityLogicalName);

            // Fetch Plug & Work security configuration
            Space.RaciReaders = CrmHelper.GetAssociatedGroups(
                adminService, entityLogicalName, subsidiaryId, mcs_configurationespacepaw_groupe_lectherit.EntityLogicalName);
            Space.RaciWriters = CrmHelper.GetAssociatedGroups(
                adminService, entityLogicalName, subsidiaryId, mcs_configurationespacepaw_groupe_contherit.EntityLogicalName);

            // Fetch Plug & Work template configuration
            TemplateId = CrmHelper.GetTemplateId(adminService, entityLogicalName, subsidiaryId);
        }
    }
}
