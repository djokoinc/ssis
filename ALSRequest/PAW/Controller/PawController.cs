﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PAW.Model;
using System.Net;

namespace PAW.Controller
{
    public class PawController
    {
        public ApiPaw api { get; set; }

        public PawController(ApiPaw apiContext)
        {
            api = apiContext;
        }
        
    }
}
