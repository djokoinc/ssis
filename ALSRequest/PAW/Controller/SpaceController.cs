﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PAW.Model;
using System.Net;

namespace PAW.Controller
{
    class SpaceController : PawController
    {
        public SpaceController(ApiPaw api) : base(api)
        {
        }

        public Space GetSpace(string spaceId)
        {

            HttpWebRequest request = api.GetHttpRequest(string.Format(ApiPaw.Route.SPACE_BY_ID, spaceId));
            request.Method = "GET";

            Space space = new Space();
            Request.GetResponse(request, out space);

            return (space.id != null) ? space : null;
        }

        public Space GetSpaceByCriteria( Dictionary<string,string> criteria )
        {

            HttpWebRequest request = api.GetHttpRequestWithCriteria(ApiPaw.Route.SPACE_SEARCH, criteria);
            request.Method = "GET";

            GetSpaceResponse spaceSearch = new GetSpaceResponse();
            Request.GetResponse(request, out spaceSearch);
            if (spaceSearch == null)
                return null;

            return spaceSearch.getUniqueResult();
        }


        public Space GetSpaceByUG(string numUg)
        {
            Dictionary<string, string> criteria = new Dictionary<string, string>();
            criteria.Add("superfields[type]", "UG");
            criteria.Add("superfields[UG]", numUg);

            HttpWebRequest request = api.GetHttpRequestWithCriteria(ApiPaw.Route.SPACE_SEARCH, criteria);
            request.Method = "GET";

            GetSpaceResponse spaceSearch = new GetSpaceResponse();
            Request.GetResponse(request, out spaceSearch);

            if (spaceSearch == null)
                return null;

            if (spaceSearch.count > 1)
            {
                Console.WriteLine(string.Format("GetUGSpaceByName get more than one result ( count = {0} ) for UG : {1}.",
                    spaceSearch.count,
                    numUg));
            }

            if (spaceSearch.count < 1)
            {
                Console.WriteLine(string.Format("GetUGSpaceByName no result found for UG : {0}.",
                    numUg));
            }

            return spaceSearch.getUniqueResult();
        }
    }
}
