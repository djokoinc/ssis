﻿using PAW.Model;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Net;

namespace PAW.Controller
{
    class DocsController : PawController
    {
        public DocsController(ApiPaw apiContext) : base(apiContext)
        {
        }

        public List<Doc> GetDocs(Space space)
        {
            if (space.id == null)
            {
                throw new Exception("GetDocsByBlocknameInSpace : space parameter have an Id");
            }

            List<Doc> docs = new List<Doc>();

            HttpWebRequest docsRequest = api.GetHttpRequest(string.Format(ApiPaw.Route.DOCS_BY_SPACE, space.id));
            docsRequest.Method = "GET";

            DocResponse docResp = new DocResponse();
            Request.GetResponse(docsRequest, out docResp);

            if (docResp.Count > 0)
            {
                docs = docResp.Results.ToList();
            }

            return docs;
        }

        public List<Doc> GetDocsByBlocknameInSpace(Space space, string blockName)
        {
            if(space.id == null)
            {
                throw new Exception("GetDocsByBlocknameInSpace : space parameter have an Id");
            }

            if (string.IsNullOrEmpty(blockName))
            {
                throw new Exception("blockname Can't be null");
            }
            List<Doc> docs = new List<Doc>();
            string blocId = space.GetBlocIdByName(blockName);

            DocsBySpaceRequest docsRequest = new DocsBySpaceRequest(blocId);
            docsRequest.httpRequest = api.GetHttpRequest(string.Format(ApiPaw.Route.DOCS_BY_SPACE, space.id), docsRequest);
            docsRequest.httpRequest.Method = "GET";

            DocResponse docResp = new DocResponse();
            docsRequest.GetResponse(out docResp);

            if(docResp.Count > 0)
            {
                docs = docResp.Results.ToList();
            }

            return docs;
        }
    }
}
