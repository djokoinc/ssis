﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace PAW.Model
{
    public class ApiPaw
    {

        private const string URL_BASE = "https://pawrec.astrimmo.com"; // REC
        
        public string urlBase { get; set; }

        public string username { get; set; }

        public string password { get; set; }

        public string token { get; set; }

        public string uuid { get; set; }

        public ApiPaw(string username, string password, string url)
        {
            if (string.IsNullOrEmpty(username))
            {
                throw new ArgumentException("ApiPaw constructor : username parameter can't be null or empty", "username");
            }

            if (string.IsNullOrEmpty(password))
            {
                throw new ArgumentException("ApiPaw constructor : password parameter can't be null or empty", "password");
            }

            if (string.IsNullOrEmpty(url))
            {
                throw new ArgumentException("ApiPaw constructor : url parameter can't be null or empty", "url");
            }

            this.username = username;
            this.password = password;
            this.urlBase = url;
                
        }

        public ApiPaw(string username, string password)
        {
            if (string.IsNullOrEmpty(username))
            {
                throw new ArgumentException("ApiPaw constructor : username parameter can't be null or empty", "username");
            }

            if (string.IsNullOrEmpty(password))
            {
                throw new ArgumentException("ApiPaw constructor : password parameter can't be null or empty", "password");
            }

            this.username = username;
            this.password = password;
            this.urlBase = URL_BASE;                
        }        

        public void SetToken() {

            if (string.IsNullOrEmpty(token) || string.IsNullOrEmpty(uuid))
            {
                GetNewToken();
            }

        }

        public void SetToken(string token , string uuid)
        {

            if (string.IsNullOrEmpty(token))
            {
                throw new Exception();
            }

            if (string.IsNullOrEmpty(uuid))
            {
                throw new Exception();
            }

            // @TODO set token and uuid for unique use 
        }

        public void GetNewToken()
        {
            if (string.IsNullOrEmpty(username))
            {
                throw new Exception("username can't be null or empty");
            }
            if (string.IsNullOrEmpty(password))
            {
                throw new Exception("password can't be null or empty");
            }

            LoginRequest tokenRequest = new LoginRequest(username,password);
            tokenRequest.httpRequest = GetHttpRequest(Route.TOKEN, tokenRequest, false);

            LoginResponse tokenResponse = new LoginResponse();
            tokenRequest.GetResponse( out tokenResponse);

            if(tokenResponse == null || string.IsNullOrEmpty(tokenResponse.authToken))
            {
                throw new Exception("Can't retrieve token");
            }

            token =  tokenResponse.authToken;
            uuid = tokenResponse.uuid;
        }

        

        public HttpWebRequest GetHttpRequest(string url, Request req, bool withToken = true)
        {
            if (withToken && string.IsNullOrEmpty(token))
            {
                GetNewToken();
            }

            string requestUrl = string.Format("{0}{1}?{2}", 
                urlBase,
                url,
                req.GetAsUrlParameter() );

            requestUrl += "&auth_token=" + token + "&uuid=" + uuid;

            HttpWebRequest httpRequest = WebRequest.CreateHttp(requestUrl);
            httpRequest.Method = "POST";
            httpRequest.ContentType = ContentType.URL_ENCODED;            

            return httpRequest;
        }

        public HttpWebRequest GetHttpRequest(string url, bool withToken = true)
        {
            if (withToken && string.IsNullOrEmpty(token))
            {
                GetNewToken();
            }

            string requestUrl = string.Format("{0}{1}",
                urlBase,
                url);

            requestUrl += "?auth_token=" + token + "&uuid=" + uuid;

            HttpWebRequest httpRequest = WebRequest.CreateHttp(requestUrl);
            httpRequest.Method = "POST";
            httpRequest.ContentType = ContentType.URL_ENCODED;

            return httpRequest;
        }

        public HttpWebRequest GetHttpRequestWithCriteria(string url, Dictionary<string, string> criteria, bool withToken = true)
        {
            if (withToken && string.IsNullOrEmpty(token))
            {
                GetNewToken();
            }

            string requestUrl = string.Format("{0}{1}",
                urlBase,
                url);

            requestUrl += "?auth_token=" + token + "&uuid=" + uuid;

            if (criteria != null || criteria.Count > 0)
            {
                requestUrl += "&" + string.Join("&", criteria.Select(x => string.Concat(
                    Uri.EscapeDataString(x.Key), "=",
                    Uri.EscapeDataString(x.Value.ToString()))));
            }

            HttpWebRequest httpRequest = WebRequest.CreateHttp(requestUrl);
            httpRequest.Method = "POST";
            httpRequest.ContentType = ContentType.URL_ENCODED;

            return httpRequest;
        }

        internal static class Route
        {
            public const string TOKEN = "/api/login";
            public const string SPACE_BY_ID = "/api/d1/spaces/{0}/";
            public const string SPACE_SEARCH = "/api/d1/spaces";
            public const string DOCS_BY_SPACE = "/api/d1/spaces/{0}/docs";
            public const string DOCS = "/api/d1/docs/";
        }

        internal static class ContentType
        {
            public const string JSON = "application/jsons";
            public const string JSON_UTF8 = "application/json; charset=UTF-8";
            public const string URL_ENCODED = "application/x-www-form-urlencoded";
        }
    }    
}
