﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PAW.Model
{
    public class Doc
    {
        [JsonProperty("id")]
        public string id { get; set; }

        [JsonProperty("title")]
        public string title { get; set; }

        [JsonProperty("created_at")]
        public DateTime createdAt { get; set; }

        [JsonProperty("updated_at")]
        public DateTime updatedAt { get; set; }

        [JsonProperty("content_type")]
        public string contentType { get; set; }

        [JsonProperty("file_md5")]
        public string fileMd5 { get; set; }

        [JsonProperty("tags")]
        public string[] tags { get; set; }

        [JsonProperty("percent")]
        public long percent { get; set; }

        [JsonProperty("type")]
        public string type { get; set; }

        [JsonProperty("tagset_ids")]
        public string[] tagsetIds { get; set; }

        [JsonProperty("responsible")]
        public string responsible { get; set; }

        [JsonProperty("writers")]
        public string[] writers { get; set; }

        [JsonProperty("readers")]
        public string[] readers { get; set; }

        [JsonProperty("thumb_url")]
        public string thumbUrl { get; set; }

        [JsonProperty("preview_url")]
        public string previewUrl { get; set; }

        [JsonProperty("thumbs_urls")]
        public string[] thumbsUrls { get; set; }

        [JsonProperty("pages_urls")]
        public string[] pagesUrls { get; set; }

        [JsonProperty("pages_count")]
        public long pagesCount { get; set; }

        [JsonProperty("superfields")]
        public Superfield superfields { get; set; }
    }
}
