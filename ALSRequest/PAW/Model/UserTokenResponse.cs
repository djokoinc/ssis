﻿using Newtonsoft.Json;

namespace PAW.Model
{
    public class UserTokenResponse : Response
    {
        [JsonProperty("success")]
        public string success { get; set; }

        [JsonProperty("info")]
        public string info { get; set; }

        [JsonProperty("login")]
        public string login { get; set; }

        [JsonProperty("email")]
        public string email { get; set; }

        [JsonProperty("user_token")]
        public string userToken { get; set; }

        [JsonProperty("device_uuid")]
        public string deviceUuid { get; set; }

        [JsonProperty("device_auth_token")]
        public string deviceAuthToken { get; set; }
    }
}
