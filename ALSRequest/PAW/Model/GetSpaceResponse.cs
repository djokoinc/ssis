﻿using Newtonsoft.Json;

namespace PAW.Model
{
    public class GetSpaceResponse : Response
    {
        [JsonProperty("count")]
        public int count { get; set; }

        [JsonProperty("limit")]
        public int limit { get; set; }

        [JsonProperty("results")]
        public Space[] pawSpaces { get; set; }

        [JsonProperty("page")]
        public int page { get; set; }

        public Space getUniqueResult()
        {

            if (count != 1)
            {
                return null;
            }

            Space returnedSpace = null;

            if ( pawSpaces != null
                && pawSpaces.Length == 1)
            {
                returnedSpace = pawSpaces[0];
            }

            return returnedSpace;
        }
    }
}
