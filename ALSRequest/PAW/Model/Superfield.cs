﻿using Newtonsoft.Json;

namespace PAW.Model
{
    public class Superfield
    {
        [JsonProperty("type")]
        public string type { get; set; }

        [JsonProperty("HP1")]
        public string hp1 { get; set; }

        [JsonProperty("HP2")]
        public string hp2 { get; set; }

        [JsonProperty("HP3")]
        public string hp3 { get; set; }

        [JsonProperty("HP4")]
        public string hp4 { get; set; }

        [JsonProperty("HP4_comple")]
        public string hp4Comple { get; set; }

        [JsonProperty("HO1")]
        public string ho1 { get; set; }

        [JsonProperty("HO2")]
        public string ho2 { get; set; }

        [JsonProperty("HO3")]
        public string ho3 { get; set; }
    }
}