﻿using Newtonsoft.Json;

namespace PAW.Model
{
    class DocsBySpaceRequest : Request
    {
        [JsonProperty("bloc_id")]
        public string blockId { get; set; }

        public DocsBySpaceRequest(string blockId)
        {
            this.blockId = blockId;
        }
    }
}
