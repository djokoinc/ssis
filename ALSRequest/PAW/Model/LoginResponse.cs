﻿using Newtonsoft.Json;

namespace PAW.Model
{
    public class LoginResponse : Response
    {
        [JsonProperty("success")]
        public string success { get; set; }

        [JsonProperty("info")]
        public string info { get; set; }

        [JsonProperty("email")]
        public string email { get; set; }

        [JsonProperty("login")]
        public string login { get; set; }

        [JsonProperty("channel_key")]
        public string channelKey { get; set; }

        [JsonProperty("uuid")]
        public string uuid { get; set; }

        [JsonProperty("auth_token")]
        public string authToken { get; set; }
    }
}
