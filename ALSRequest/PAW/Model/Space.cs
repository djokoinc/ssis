﻿using Newtonsoft.Json;
using System;
using System.Linq;

namespace PAW.Model
{
    public class Space
    {
        [JsonProperty("id")]
        public string id { get; set; }

        [JsonProperty("title")]
        public string title { get; set; }

        [JsonProperty("created_at")]
        public System.DateTime createdAt { get; set; }

        [JsonProperty("updated_at")]
        public System.DateTime updatedAt { get; set; }

        [JsonProperty("tags")]
        public string[] tags { get; set; }

        [JsonProperty("percent")]
        public long percent { get; set; }

        [JsonProperty("writers")]
        public string[] writers { get; set; }

        [JsonProperty("readers")]
        public string[] readers { get; set; }

        [JsonProperty("raci_template")]
        public object raciTemplate { get; set; }

        [JsonProperty("raci_enforce")]
        public object raciEnforce { get; set; }

        [JsonProperty("raci_writers")]
        public string[] raciWriters { get; set; }

        [JsonProperty("raci_readers")]
        public string[] raciReaders { get; set; }

        [JsonProperty("informed")]
        public string[] informed { get; set; }

        [JsonProperty("followers")]
        public string[] followers { get; set; }

        [JsonProperty("responsible")]
        public string responsible { get; set; }

        [JsonProperty("superfields")]
        public Superfield superfields { get; set; }

        [JsonProperty("blocset_id")]
        public string blocsetId { get; set; }

        [JsonProperty("blocs_ids")]
        public string[] blocsIds { get; set; }

        [JsonProperty("blocs_names")]
        public string[] blocsNames { get; set; }

        [JsonProperty("tagset_ids")]
        public object[] tagsetIds { get; set; }

        public string GetBlocIdByName(string name)
        {
            if (blocsIds == null || blocsNames == null 
                || blocsIds.Count() < 1 || blocsNames.Count() > 1)
            {
                return null;
            }

            if (!blocsNames.Contains(name))
            {
                return null;
            }

            int index = Array.IndexOf(blocsNames, name);
            return (string)blocsIds.GetValue(index);
        }

    }
}
