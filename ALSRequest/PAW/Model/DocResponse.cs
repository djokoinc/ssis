﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PAW.Model
{
    class DocResponse
    {
        [JsonProperty("results")]
        public Doc[] Results { get; set; }

        [JsonProperty("count")]
        public long Count { get; set; }

        [JsonProperty("limit")]
        public long Limit { get; set; }

        [JsonProperty("page")]
        public int Page { get; set; }
    }
}
