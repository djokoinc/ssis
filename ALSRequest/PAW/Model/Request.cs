﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace PAW.Model
{
    public class Request
    {
        /// <summary>
        /// HttpRequest pass to the request
        /// </summary>
        public HttpWebRequest httpRequest { get; set; }
                
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GetAsUrlParameter()
        {
            string separator = ",";

            var oData = this;

            // Get all properties on the object
            var properties = oData.GetType().GetProperties()
                .Where(x => x.CanRead )
                .Where(x => x.GetValue(oData, null) != null)
                .ToDictionary(x => x.GetCustomAttributes<JsonPropertyAttribute>().First().PropertyName , x => x.GetValue(oData, null));

            // Get names for all IEnumerable properties (excl. string)
            var propertyNames = properties
                .Where(x => !(x.Value is string) && x.Value is IEnumerable)
                .Select(x => x.Key)
                .ToList();
            
            // Concat all IEnumerable properties into a comma separated string
            foreach (var key in propertyNames)
            {
                var valueType = properties[key].GetType();
                var valueElemType = valueType.IsGenericType
                                        ? valueType.GetGenericArguments()[0]
                                        : valueType.GetElementType();
                if (valueElemType.IsPrimitive || valueElemType == typeof(string))
                {
                    var enumerable = properties[key] as IEnumerable;
                    properties[key] = string.Join(separator, enumerable.Cast<object>());
                }
            }

            // Concat all key/value pairs into a string separated by ampersand
            return string.Join("&", properties
                .Select(x => string.Concat(
                    Uri.EscapeDataString(x.Key), "=",
                    Uri.EscapeDataString(x.Value.ToString()))));
        }

        public void GetResponse<T>( out T res) {

            if (httpRequest == null)
            {
                throw new Exception("GetResponse httpRequest property of Request must be set before!");
            }

            try
            {
                using (HttpWebResponse response = (HttpWebResponse)httpRequest.GetResponse())
                {
                    using (Stream stream = response.GetResponseStream())
                    using (StreamReader streamReader = new StreamReader(stream))
                    {
                        string jsonStr = streamReader.ReadToEnd().ToString();
                        res = JsonConvert.DeserializeObject<T>(jsonStr);
                    };

                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        throw new Exception(string.Format("Error on PAW Request"
                            + " --> HttpStatusCode : {0}", response.StatusCode));
                    }
                }
            } catch (WebException e)
            {
                // @TODO
                throw new Exception(e.Message);
            } catch (Exception e )
            {
                throw new Exception(e.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="req"></param>
        /// <param name="res"></param>
        public static void GetResponse<T>(HttpWebRequest req, out T res)
        {

            if (req == null)
            {
                throw new Exception("GetResponse httpRequest property of Request must be set before!");
            }

            try
            {
                using (HttpWebResponse response = (HttpWebResponse)req.GetResponse())
                {
                    using (Stream stream = response.GetResponseStream())
                    using (StreamReader streamReader = new StreamReader(stream))
                    {
                        res = JsonConvert.DeserializeObject<T>(streamReader.ReadToEnd().ToString());
                    };

                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        throw new Exception(string.Format("Error on PAW Request"
                            + " --> HttpStatusCode : {0}", response.StatusCode));
                    }
                }
            }
            catch (WebException e)
            {
                // @TODO Not the rigth to do that
                throw new Exception(e.Message);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public string GetResponseAsString()
        {
            if (httpRequest == null)
            {
                throw new Exception("GetResponse httpRequest property of Request must be set before!");
            }

            try
            {
                using (HttpWebResponse response = (HttpWebResponse)httpRequest.GetResponse())
                {
                    using (Stream stream = response.GetResponseStream())
                    using (StreamReader streamReader = new StreamReader(stream))
                    {
                        return streamReader.ReadToEnd().ToString();
                    };
                }
            }
            catch (WebException e)
            {
                // @TODO
                throw e;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        
    }
}
