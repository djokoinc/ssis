﻿using Newtonsoft.Json;

namespace PAW.Model
{
    public class LoginRequest : Request
    {
        [JsonProperty("username")]
        public string userName { get; set; }

        [JsonProperty("password")]
        public string password { get; set; }

        [JsonProperty("uuid")]
        public string uuid { get; set; }

        public LoginRequest(string userName, string password)
        {
            this.userName = userName;
            this.password = password;
        }

        public LoginRequest(string userName, string password, string uuid)
        {
            this.userName = userName;
            this.password = password;
            this.uuid = uuid;
        }


    }
}
