﻿using System;
using System.Collections.Generic;
using System.Configuration;
using PAW.Model;
using PAW.Controller;

namespace ALSRequest
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("================START================");
            string mdp = "AZRNJU1536@!";
            string user = "adminpaw-dcrm";
            string ugNumber = "312557";
            string uuid = "D365-dev";
            string pawUrl = "https://paw.astrimmo.com";
            //string spaceId = "59f349d304300279a8e3e09e";
            //string blocPhotos = "Photos commercialisées";

            ApiPaw api = new ApiPaw(user, mdp);
            //api.uuid = uuid;
            //api.SetToken();
            //Console.WriteLine("================Token================");
            //Console.WriteLine(string.Format("token => {0} for user : {1}",
            //    api.token, api.username));

            //SpaceController spaceC = new SpaceController(api);
            //Space space = spaceC.GetSpace(spaceId);

            //Console.WriteLine("================Space================");
            //Console.WriteLine(string.Format("Space => {0} , Title : '{1}' ",
            //    space.id, space.title));


            //Space spaceByUG = spaceC.GetSpaceByUG(ugNumber);
            //if (spaceByUG != null)
            //{
            //    Console.WriteLine("================SpaceByUG============");
            //    Console.WriteLine(string.Format("Space => {0} , Title : '{1}' ",
            //        spaceByUG.id, spaceByUG.title));
            //}else
            //{
            //    Console.ReadKey();
            //    return;
            //}

            //string spaceComName = "COM " + spaceByUG.title;

            //Dictionary<string, string> criteria = new Dictionary<string, string>();

            //criteria.Add("title", spaceComName);
            //criteria.Add("exact", "true");


            //Space spaceCom = spaceC.GetSpaceByCriteria(criteria);
            //if (spaceCom != null)
            //{
            //    Console.WriteLine("================SpaceCom=============");
            //    Console.WriteLine(string.Format("Space => {0} , Title : '{1}' ",
            //        spaceCom.id, spaceCom.title));
            //    Console.WriteLine(string.Format("Bloc in : {0}{1}",
            //        Environment.NewLine, string.Join(Environment.NewLine, spaceCom.blocsNames) ));
            //}
            //else
            //{
            //    Console.WriteLine("No space found for title : " + spaceComName);
            //    Console.ReadKey();
            //    return;
            //}



            Console.WriteLine("====================================");
            Console.WriteLine("================Test PROD===========");
            Console.WriteLine("====================================");

            ApiPaw apiProd = new ApiPaw(user, mdp, pawUrl);
            
            apiProd.uuid = uuid;
            apiProd.SetToken();
            Console.WriteLine("================Token================");
            Console.WriteLine(string.Format("token => {0} for user : {1}",
                apiProd.token, apiProd.username));
            
            SpaceController spaceCProd = new SpaceController(apiProd);

            Dictionary<string,string> testCriteria = new Dictionary<string, string>();
            testCriteria.Add("title", ugNumber);
            testCriteria.Add("superfields[type]", "COM");

            Space COM = spaceCProd.GetSpaceByCriteria(testCriteria);
            if(COM == null)
            {
                Console.WriteLine("NOOOOOOOOOOOOOOOOOOO");
            }else
            {
                Console.WriteLine("================SpaceByUG============");
                Console.WriteLine(string.Format("Space => {0} , Title : '{1}' ",
                    COM.id, COM.title));
            }


            DocsController docC = new DocsController(apiProd);
            List<Doc> docs = docC.GetDocs(COM);

            
            foreach(Doc doc in docs)
            {
                Console.WriteLine(
                    string.Format("=> Name :{0} | thumbUrl: {1} | previewUrl: {2} | thumbsUrls : {3}",
                        doc.title,
                        doc.thumbUrl,
                        doc.previewUrl,
                        string.Format(",", doc.thumbsUrls)
                    ));
            }

            Console.WriteLine();
            
            
            Console.ReadKey();
        }
    }   
}
