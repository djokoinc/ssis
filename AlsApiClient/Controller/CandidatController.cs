﻿using AlsApi.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace AlsApi.Controller
{
    public class CandidatController : Controller
    {
        public CandidatController(ApiCallContext context) : base(context)
        {
        }

        public CandidatController(string username, string password) : base(username, password)
        {
        }

        public CandidatController(string url, string username, string password) : base(url,username, password)
        {
        }

        public List<CandidatALS> GetByDemande(DemandeCriteria criteria)
        {            
            HttpWebRequest candidatRequest = callContext.GetWebRequest(ApiCallContext.Route.LOGEMENT_DEMANDE, criteria, 10000, 0);
            Response res = callContext.GetResponse(candidatRequest);
            return res.content;
        }

        public List<CandidatALS> GetByTitulaire(TitulaireCriteria criteria)
        {
            return null;
        }
    }
}
