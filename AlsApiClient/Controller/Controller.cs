﻿using System;
using System.Collections.Generic;
using System.IO;
using AlsApi.Model;
using System.Linq;
using System.Net;
using System.Net.Cache;
using System.Text;
using System.Threading.Tasks;

namespace AlsApi.Controller
{
    public class Controller
    {
        public ApiCallContext callContext { get; set; }
        
        public Controller(ApiCallContext apiContext)
        {
            callContext = apiContext;
        }

        public Controller(string username, string password)
        {
            callContext = new ApiCallContext(username, password);
        }

        public Controller(string urlBase, string username, string password)
        {
            callContext = new ApiCallContext(username, password, urlBase);
        }       
    }
}
