﻿

#region Namespaces
using System;
using System.Data;
using Microsoft.SqlServer.Dts.Pipeline.Wrapper;
using Microsoft.SqlServer.Dts.Runtime.Wrapper;
using System.Net;
using AlsApi.Controller;
using AlsApi.Model;
using System.Collections.Generic;
using System.Windows.Forms;
#endregion

/// <summary>
/// This is the class to which to add your code.  Do not change the name, attributes, or parent
/// of this class.
/// </summary>
[Microsoft.SqlServer.Dts.Pipeline.SSISScriptComponentEntryPointAttribute]
public class ScriptMain : UserComponent
{
    #region Help:  Using Integration Services variables and parameters
    /* To use a variable in this script, first ensure that the variable has been added to
     * either the list contained in the ReadOnlyVariables property or the list contained in
     * the ReadWriteVariables property of this script component, according to whether or not your
     * code needs to write into the variable.  To do so, save this script, close this instance of
     * Visual Studio, and update the ReadOnlyVariables and ReadWriteVariables properties in the
     * Script Transformation Editor window.
     * To use a parameter in this script, follow the same steps. Parameters are always read-only.
     *
     * Example of reading from a variable or parameter:
     *  DateTime startTime = Variables.MyStartTime;
     *
     * Example of writing to a variable:
     *  Variables.myStringVariable = "new value";
     */
    #endregion

    #region Help:  Using Integration Services Connnection Managers
    /* Some types of connection managers can be used in this script component.  See the help topic
     * "Working with Connection Managers Programatically" for details.
     *
     * To use a connection manager in this script, first ensure that the connection manager has
     * been added to either the list of connection managers on the Connection Managers page of the
     * script component editor.  To add the connection manager, save this script, close this instance of
     * Visual Studio, and add the Connection Manager to the list.
     *
     * If the component needs to hold a connection open while processing rows, override the
     * AcquireConnections and ReleaseConnections methods.
     * 
     * Example of using an ADO.Net connection manager to acquire a SqlConnection:
     *  object rawConnection = Connections.SalesDB.AcquireConnection(transaction);
     *  SqlConnection salesDBConn = (SqlConnection)rawConnection;
     *
     * Example of using a File connection manager to acquire a file path:
     *  object rawConnection = Connections.Prices_zip.AcquireConnection(transaction);
     *  string filePath = (string)rawConnection;
     *
     * Example of releasing a connection manager:
     *  Connections.SalesDB.ReleaseConnection(rawConnection);
     */
    #endregion

    #region Help:  Firing Integration Services Events
    /* This script component can fire events.
     *
     * Example of firing an error event:
     *  ComponentMetaData.FireError(10, "Process Values", "Bad value", "", 0, out cancel);
     *
     * Example of firing an information event:
     *  ComponentMetaData.FireInformation(10, "Process Values", "Processing has started", "", 0, fireAgain);
     *
     * Example of firing a warning event:
     *  ComponentMetaData.FireWarning(10, "Process Values", "No rows were received", "", 0);
     */
    #endregion

    public ApiCallContext apiALS { get; set; }

    /// <summary>
    /// This method is called once, before rows begin to be processed in the data flow.
    ///
    /// You can remove this method if you don't need to do anything here.
    /// </summary>
    public override void PreExecute()
    {
        base.PreExecute();

        string url = Variables.PlatineUrl;
        string user = Variables.PlatineUsername;
        string pwd = Variables.PlatinePassword;

        apiALS = new ApiCallContext(user, pwd, url);
        if(apiALS.tokenJWT == null)
        {
            throw new Exception("No token retrieve from ALS");
        }
        
    }

    /// <summary>
    /// This method is called after all the rows have passed through this component.
    ///
    /// You can delete this method if you don't need to do anything here.
    /// </summary>
    public override void PostExecute()
    {
        base.PostExecute();
        /*
         * Add your code here
         */
    }

    /// <summary>
    /// This method is called once for every row that passes through the component from InputMcsug.
    ///
    /// Example of reading a value from a column in the the row:
    ///  string zipCode = Row.ZipCode
    ///
    /// Example of writing a value to a column in the row:
    ///  Row.ZipCode = zipCode
    /// </summary>
    /// <param name="Row">The row that is currently passing through the component</param>
    public override void InputMcsug_ProcessInputRow(InputMcsugBuffer Row)
    {
        CandidatController apiCandidat = new CandidatController(apiALS);
        DemandeCriteria criteria = SetCriteriaFromRow(Row);

        if (criteria == null)
        {
            FillErrorOutputFromPetrus(Row);
            return;
        }

        List<CandidatALS> candidats = apiCandidat.GetByDemande(criteria);
        if(candidats == null || candidats.Count < 1)
        {
            return;
        }
        
        bool candidatOut = FillOutputCandidat(candidats, Row.mcsugid, Row.mcsname, criteria);

        if (candidatOut)
        {
            FillOutputUg(Row.mcsname, Row.mcsugid);
        } else
        {
            FillOutputErrorUgWithNoValidLead(Row, criteria);
        }
    }

    public override void CreateNewOutputRows()
    {
        /*
          Add rows by calling the AddRow method on the member variable named "<Output Name>Buffer".
          For example, call MyOutputBuffer.AddRow() if your output was named "MyOutput".
        */
    }

    #region Handle Inputed row before request ALS

    public DemandeCriteria SetCriteriaFromRow(InputMcsugBuffer row)
    {

        DemandeCriteria criteria = new DemandeCriteria();

        if (UgHasWrongMandatoryValue(row))
        {
            return null;
        }
        
        // mandatory value
        criteria.actif = true;
        criteria.emailPresent = true;

        // Handle Typo correspondence
        if (criteria.getTypoCorrespondence(row.mcspiececodename) == null)
            return null;

        criteria.typologies = new List<string>() { criteria.getTypoCorrespondence(row.mcspiececodename) };
        criteria.codePostal = row.mcscodepostalug;
        criteria.ressourcesMensuellesMin = (int)row.mcsloyerappartementcur * 3;

        // @TODO when rules are clear 
        //criteria.compositionFamiliale = ?;

        return criteria;
    }

    public void FillOutputUg(string name, Guid ugId)
    {
        OutputUgBuffer.AddRow();

        if (string.IsNullOrEmpty(name))
            name = "";

        if (ugId == null)
            ugId = Guid.Empty;

        OutputUgBuffer.UGmcsugid = ugId;
        OutputUgBuffer.UGmcsname = name;

    }

    public bool UgHasWrongMandatoryValue(InputMcsugBuffer row)
    {
        return (row.mcsugid_IsNull
            || row.mcsname_IsNull
            || row.mcsloyerappartementcur_IsNull
            || row.mcspiececodename_IsNull
            || row.mcscodepostalug_IsNull
            || row.statecode_IsNull);
    }

    public void FillErrorOutputFromPetrus(InputMcsugBuffer row)
    {
        OutputErrorFromPetrusBuffer.AddRow();
        
        if (!row.mcsugid_IsNull)
            OutputErrorFromPetrusBuffer.errorUGid = row.mcsugid;
        
        string errorDesc = "InputMcsugBuffer check error:{0}- mcsugid_IsNull : {1}"
            + "{0}- mcsname_IsNull : {2}"
            + "{0}- mcsloyerappartementcur_IsNull : {3}"
            + "{0}- mcspiececodename_IsNull : {4}"
            + "{0}- mcscodepostalug_IsNull : {5}"
            + "{0}- statecode_IsNull : {6}";

        OutputErrorFromPetrusBuffer.errordesc = string.Format(errorDesc,
            Environment.NewLine,
            row.mcsugid_IsNull,
            row.mcsname_IsNull,
            row.mcsloyerappartementcur_IsNull,
            row.mcspiececodename_IsNull,
            row.mcscodepostalug_IsNull,
            row.statecode_IsNull);

        if(!row.mcsikosid_IsNull)
            OutputErrorFromPetrusBuffer.errorikosid = row.mcsikosid;
    }

    #endregion


    #region Handle Candidat from ALS
    public bool CandidatHasWrongMandatoryValue(CandidatALS candidat)
    {
        bool error = false;       

        if (string.IsNullOrEmpty(candidat.nom)
            || string.IsNullOrEmpty(candidat.prenom)
            || candidat.emails == null
            || candidat.emails.Count == 0
            || candidat.telephones == null
            || candidat.telephones.Count == 0)
        {
            error = true;
        }

        if (candidat.IsOneValidMails())
        {
            error = true;
        }

        return error;
    }

    public bool FillOutputCandidat(List<CandidatALS> candidats, Guid ugId, string ugName, Criteria requestCriteria)
    {
        if(ugId == null || ugId == Guid.Empty)
        {
            throw new Exception("FillOutputCandidat ugId parameter can't be empty or null");
        }

        if(candidats == null || candidats.Count < 1)
        {
            throw new Exception("FillOutputCandidat candidats parameter can't be empty or null");
        }

        bool candidatOut = false;
        foreach ( var candidat in candidats)
        {
            if (CandidatHasWrongMandatoryValue(candidat))
            {
                FillErrorOutputFromAls(candidat, ugId, requestCriteria);
                continue;
            }

            OutputCandidatBuffer.AddRow();
            OutputCandidatBuffer.LEADlastname = candidat.nom;
            OutputCandidatBuffer.LEADfristname = candidat.prenom;

            bool email1ToFill = true , email2ToFill = true;

            foreach(string email in candidat.emails)
            {
                if( !(email1ToFill && email2ToFill))
                {
                    break;
                }

                if (CandidatALS.IsValidMail(email) && email1ToFill)
                {
                    OutputCandidatBuffer.LEADemailaddress1 = email;
                }
                else if(CandidatALS.IsValidMail(email) && email2ToFill)
                {
                    OutputCandidatBuffer.LEADemailaddress2 = email;
                }
            }
            
            OutputCandidatBuffer.UGname = ugName;
            OutputCandidatBuffer.UGid = ugId;
            OutputCandidatBuffer.LEADnocrystal = candidat.numeroCristal;
            candidatOut = true;
        }

        return candidatOut;        
    }

    public void FillErrorOutputFromAls(CandidatALS candidat, Guid ugId , Criteria requestCriteria)
    {
        OutputErrorFromAlsBuffer.AddRow();
        OutputErrorFromAlsBuffer.errorUGid = ugId;

        string errorDesc = "CandidatALS check error:{0}- Prenom is null: {1}"
               + "{0}- nom  is null : {2}"
               + "{0}- emails is empty : {3}"
               + "{0}  emails have no valid format : {4}"
               + "{0}- telephones is empty : {5}";

        OutputErrorFromAlsBuffer.errordescleadals = string.Format(errorDesc,
            Environment.NewLine,
            string.IsNullOrEmpty(candidat.prenom),
            string.IsNullOrEmpty(candidat.nom),
            (candidat.emails == null || candidat.emails.Count == 0),
            candidat.IsOneValidMails(),
            (candidat.telephones == null || candidat.telephones.Count == 0));

        // ADD here Criteria description
        OutputErrorFromAlsBuffer.requestcriteria = requestCriteria.ToJSONString();
    }

    public void FillOutputErrorUgWithNoValidLead(InputMcsugBuffer row, Criteria requestCriteria)
    {
        string DESC = "Aucun prospect valide venant d'ALS";

        OutputErrorUgWithNoValidLeadBuffer.AddRow();
        OutputErrorUgWithNoValidLeadBuffer.errordesc = DESC;

        if (!row.mcsikosid_IsNull)
            OutputErrorUgWithNoValidLeadBuffer.errorikosid = row.mcsikosid;

        if (!row.mcsugid_IsNull)
            OutputErrorUgWithNoValidLeadBuffer.errorUGid = row.mcsugid;

        OutputErrorUgWithNoValidLeadBuffer.requestcriteria = requestCriteria.ToJSONString();

    }

    #endregion


}
