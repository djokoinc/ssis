﻿/* THIS IS AUTO-GENERATED CODE THAT WILL BE OVERWRITTEN! DO NOT EDIT!
*  Microsoft SQL Server Integration Services component wrapper
*  This module defines the base class for your component
*  THIS IS AUTO-GENERATED CODE THAT WILL BE OVERWRITTEN! DO NOT EDIT! */

using System;
using System.Data;
using Microsoft.SqlServer.Dts.Pipeline;
using Microsoft.SqlServer.Dts.Pipeline.Wrapper;
using Microsoft.SqlServer.Dts.Runtime.Wrapper;

public class UserComponent: ScriptComponent
{
    public Connections Connections;
    public Variables Variables;
    public UserComponent()
    {
        Connections = new Connections(this);
        Variables = new Variables(this);
    }

    public OutputErrorFromPetrusBuffer OutputErrorFromPetrusBuffer;

    public OutputErrorUgWithNoValidLeadBuffer OutputErrorUgWithNoValidLeadBuffer;

    public OutputCandidatBuffer OutputCandidatBuffer;

    public OutputUgBuffer OutputUgBuffer;

    public OutputErrorFromAlsBuffer OutputErrorFromAlsBuffer;

    int InputsFinished = 0;

    public override void ProcessInput(int InputID, string InputName, PipelineBuffer Buffer, OutputNameMap OutputMap)
    {

        if (InputName.Equals(@"InputMcs_ug", StringComparison.Ordinal))
        {
            InputMcsug_ProcessInput(new InputMcsugBuffer(Buffer, GetColumnIndexes(InputID), OutputMap));
        }

        if (Buffer.EndOfRowset)
        {
            InputsFinished = InputsFinished + 1;
            if (InputsFinished == 1)
            {
                FinishOutputs();
                MarkOutputsFinished();
            }
        }

    }

    public virtual void FinishOutputs()
    {
    }

    private void MarkOutputsFinished()
    {

        if (null != OutputErrorFromPetrusBuffer)
        {
            OutputErrorFromPetrusBuffer.SetEndOfRowset();
            OutputErrorFromPetrusBuffer = null;
        }

        if (null != OutputErrorUgWithNoValidLeadBuffer)
        {
            OutputErrorUgWithNoValidLeadBuffer.SetEndOfRowset();
            OutputErrorUgWithNoValidLeadBuffer = null;
        }

        if (null != OutputCandidatBuffer)
        {
            OutputCandidatBuffer.SetEndOfRowset();
            OutputCandidatBuffer = null;
        }

        if (null != OutputUgBuffer)
        {
            OutputUgBuffer.SetEndOfRowset();
            OutputUgBuffer = null;
        }

        if (null != OutputErrorFromAlsBuffer)
        {
            OutputErrorFromAlsBuffer.SetEndOfRowset();
            OutputErrorFromAlsBuffer = null;
        }

    }

    public override void PrimeOutput(int Outputs, int[] OutputIDs, PipelineBuffer[] Buffers, OutputNameMap OutputMap)
    {
       for(int Idx = 0; Idx < Outputs; Idx++)
       {
            if(OutputIDs[Idx] == GetOutputID(OutputMap, @"OutputErrorFromPetrus"))
            {
                OutputErrorFromPetrusBuffer = new OutputErrorFromPetrusBuffer(Buffers[Idx], GetColumnIndexes(OutputIDs[Idx]), OutputMap);
            }
            if(OutputIDs[Idx] == GetOutputID(OutputMap, @"OutputErrorUgWithNoValidLead"))
            {
                OutputErrorUgWithNoValidLeadBuffer = new OutputErrorUgWithNoValidLeadBuffer(Buffers[Idx], GetColumnIndexes(OutputIDs[Idx]), OutputMap);
            }
            if(OutputIDs[Idx] == GetOutputID(OutputMap, @"OutputCandidat"))
            {
                OutputCandidatBuffer = new OutputCandidatBuffer(Buffers[Idx], GetColumnIndexes(OutputIDs[Idx]), OutputMap);
            }
            if(OutputIDs[Idx] == GetOutputID(OutputMap, @"OutputUg"))
            {
                OutputUgBuffer = new OutputUgBuffer(Buffers[Idx], GetColumnIndexes(OutputIDs[Idx]), OutputMap);
            }
            if(OutputIDs[Idx] == GetOutputID(OutputMap, @"OutputErrorFromAls"))
            {
                OutputErrorFromAlsBuffer = new OutputErrorFromAlsBuffer(Buffers[Idx], GetColumnIndexes(OutputIDs[Idx]), OutputMap);
            }
        }

        CreateNewOutputRows();

    }

    public virtual void CreateNewOutputRows()
    {
    }

    public virtual void InputMcsug_ProcessInput(InputMcsugBuffer Buffer)
    {
        while (Buffer.NextRow())
        {
            InputMcsug_ProcessInputRow(Buffer);
        }
    }

    public virtual void InputMcsug_ProcessInputRow(InputMcsugBuffer Row)
    {
    }

}

public class Connections
{
    ScriptComponent ParentComponent;

    public Connections(ScriptComponent Component)
    {
        ParentComponent = Component;
    }

}

public class Variables
{
    ScriptComponent ParentComponent;

    public Variables(ScriptComponent Component)
    {
        ParentComponent = Component;
    }

    public String PlatinePassword
    {
        get
        {
            return (String)(ParentComponent.ReadOnlyVariables["PlatinePassword"].GetValueWithContext(ScriptComponent.EvaluatorContext));
        }
    }

    public String PlatineUrl
    {
        get
        {
            return (String)(ParentComponent.ReadOnlyVariables["PlatineUrl"].GetValueWithContext(ScriptComponent.EvaluatorContext));
        }
    }

    public String PlatineUsername
    {
        get
        {
            return (String)(ParentComponent.ReadOnlyVariables["PlatineUsername"].GetValueWithContext(ScriptComponent.EvaluatorContext));
        }
    }

}
