﻿/* THIS IS AUTO-GENERATED CODE THAT WILL BE OVERWRITTEN! DO NOT EDIT!
*  Microsoft SQL Server Integration Services buffer wrappers
*  This module defines classes for accessing data flow buffers
*  THIS IS AUTO-GENERATED CODE THAT WILL BE OVERWRITTEN! DO NOT EDIT! */



using System;
using System.Data;
using Microsoft.SqlServer.Dts.Pipeline;
using Microsoft.SqlServer.Dts.Pipeline.Wrapper;

public class InputMcsugBuffer: ScriptBuffer

{
    public InputMcsugBuffer(PipelineBuffer Buffer, int[] BufferColumnIndexes, OutputNameMap OutputMap)
        : base(Buffer, BufferColumnIndexes, OutputMap)
    {
    }

    public String mcscodepostalug
    {
        get
        {
            return Buffer.GetString(BufferColumnIndexes[0]);
        }
    }
    public bool mcscodepostalug_IsNull
    {
        get
        {
            return IsNull(0);
        }
    }

    public String mcsvilletxt
    {
        get
        {
            return Buffer.GetString(BufferColumnIndexes[1]);
        }
    }
    public bool mcsvilletxt_IsNull
    {
        get
        {
            return IsNull(1);
        }
    }

    public Guid mcsugid
    {
        get
        {
            return Buffer.GetGuid(BufferColumnIndexes[2]);
        }
    }
    public bool mcsugid_IsNull
    {
        get
        {
            return IsNull(2);
        }
    }

    public Int32 statecode
    {
        get
        {
            return Buffer.GetInt32(BufferColumnIndexes[3]);
        }
    }
    public bool statecode_IsNull
    {
        get
        {
            return IsNull(3);
        }
    }

    public String mcspiececodename
    {
        get
        {
            return Buffer.GetString(BufferColumnIndexes[4]);
        }
    }
    public bool mcspiececodename_IsNull
    {
        get
        {
            return IsNull(4);
        }
    }

    public Int32 mcspiececode
    {
        get
        {
            return Buffer.GetInt32(BufferColumnIndexes[5]);
        }
    }
    public bool mcspiececode_IsNull
    {
        get
        {
            return IsNull(5);
        }
    }

    public String mcsname
    {
        get
        {
            return Buffer.GetString(BufferColumnIndexes[6]);
        }
    }
    public bool mcsname_IsNull
    {
        get
        {
            return IsNull(6);
        }
    }

    public Decimal mcsloyerappartementcur
    {
        get
        {
            return Buffer.GetDecimal(BufferColumnIndexes[7]);
        }
    }
    public bool mcsloyerappartementcur_IsNull
    {
        get
        {
            return IsNull(7);
        }
    }

    public String mcsikosid
    {
        get
        {
            return Buffer.GetString(BufferColumnIndexes[8]);
        }
    }
    public bool mcsikosid_IsNull
    {
        get
        {
            return IsNull(8);
        }
    }

    new public bool NextRow()
    {
        return base.NextRow();
    }

    new public bool EndOfRowset()
    {
        return base.EndOfRowset();
    }

}

public class OutputErrorFromPetrusBuffer: ScriptBuffer

{
    public OutputErrorFromPetrusBuffer(PipelineBuffer Buffer, int[] BufferColumnIndexes, OutputNameMap OutputMap)
        : base(Buffer, BufferColumnIndexes, OutputMap)
    {
    }

    public String errordesc
    {
        set
        {
            this[0] = value;
        }
    }
    public bool errordesc_IsNull
    {
        set
        {
            if (value)
            {
                SetNull(0);
            }
            else
            {
                throw new InvalidOperationException("IsNull property cannot be set to False. Assign a value to the column instead.");
            }
        }
    }

    public Guid errorUGid
    {
        set
        {
            this[1] = value;
        }
    }
    public bool errorUGid_IsNull
    {
        set
        {
            if (value)
            {
                SetNull(1);
            }
            else
            {
                throw new InvalidOperationException("IsNull property cannot be set to False. Assign a value to the column instead.");
            }
        }
    }

    public String errorikosid
    {
        set
        {
            this[2] = value;
        }
    }
    public bool errorikosid_IsNull
    {
        set
        {
            if (value)
            {
                SetNull(2);
            }
            else
            {
                throw new InvalidOperationException("IsNull property cannot be set to False. Assign a value to the column instead.");
            }
        }
    }

    new public void AddRow()
    {
        base.AddRow();
    }

    new public void SetEndOfRowset()
    {
        base.SetEndOfRowset();
    }

    new public bool EndOfRowset()
    {
        return base.EndOfRowset();
    }

}

public class OutputErrorUgWithNoValidLeadBuffer: ScriptBuffer

{
    public OutputErrorUgWithNoValidLeadBuffer(PipelineBuffer Buffer, int[] BufferColumnIndexes, OutputNameMap OutputMap)
        : base(Buffer, BufferColumnIndexes, OutputMap)
    {
    }

    public Guid errorUGid
    {
        set
        {
            this[0] = value;
        }
    }
    public bool errorUGid_IsNull
    {
        set
        {
            if (value)
            {
                SetNull(0);
            }
            else
            {
                throw new InvalidOperationException("IsNull property cannot be set to False. Assign a value to the column instead.");
            }
        }
    }

    public String errorikosid
    {
        set
        {
            this[1] = value;
        }
    }
    public bool errorikosid_IsNull
    {
        set
        {
            if (value)
            {
                SetNull(1);
            }
            else
            {
                throw new InvalidOperationException("IsNull property cannot be set to False. Assign a value to the column instead.");
            }
        }
    }

    public String requestcriteria
    {
        set
        {
            this[2] = value;
        }
    }
    public bool requestcriteria_IsNull
    {
        set
        {
            if (value)
            {
                SetNull(2);
            }
            else
            {
                throw new InvalidOperationException("IsNull property cannot be set to False. Assign a value to the column instead.");
            }
        }
    }

    public String errordesc
    {
        set
        {
            this[3] = value;
        }
    }
    public bool errordesc_IsNull
    {
        set
        {
            if (value)
            {
                SetNull(3);
            }
            else
            {
                throw new InvalidOperationException("IsNull property cannot be set to False. Assign a value to the column instead.");
            }
        }
    }

    new public void AddRow()
    {
        base.AddRow();
    }

    new public void SetEndOfRowset()
    {
        base.SetEndOfRowset();
    }

    new public bool EndOfRowset()
    {
        return base.EndOfRowset();
    }

}

public class OutputCandidatBuffer: ScriptBuffer

{
    public OutputCandidatBuffer(PipelineBuffer Buffer, int[] BufferColumnIndexes, OutputNameMap OutputMap)
        : base(Buffer, BufferColumnIndexes, OutputMap)
    {
    }

    public Guid UGid
    {
        set
        {
            this[0] = value;
        }
    }
    public bool UGid_IsNull
    {
        set
        {
            if (value)
            {
                SetNull(0);
            }
            else
            {
                throw new InvalidOperationException("IsNull property cannot be set to False. Assign a value to the column instead.");
            }
        }
    }

    public String UGname
    {
        set
        {
            this[1] = value;
        }
    }
    public bool UGname_IsNull
    {
        set
        {
            if (value)
            {
                SetNull(1);
            }
            else
            {
                throw new InvalidOperationException("IsNull property cannot be set to False. Assign a value to the column instead.");
            }
        }
    }

    public String LEADfristname
    {
        set
        {
            this[2] = value;
        }
    }
    public bool LEADfristname_IsNull
    {
        set
        {
            if (value)
            {
                SetNull(2);
            }
            else
            {
                throw new InvalidOperationException("IsNull property cannot be set to False. Assign a value to the column instead.");
            }
        }
    }

    public String LEADlastname
    {
        set
        {
            this[3] = value;
        }
    }
    public bool LEADlastname_IsNull
    {
        set
        {
            if (value)
            {
                SetNull(3);
            }
            else
            {
                throw new InvalidOperationException("IsNull property cannot be set to False. Assign a value to the column instead.");
            }
        }
    }

    public String LEADemailaddress2
    {
        set
        {
            this[4] = value;
        }
    }
    public bool LEADemailaddress2_IsNull
    {
        set
        {
            if (value)
            {
                SetNull(4);
            }
            else
            {
                throw new InvalidOperationException("IsNull property cannot be set to False. Assign a value to the column instead.");
            }
        }
    }

    public String LEADmobilephone
    {
        set
        {
            this[5] = value;
        }
    }
    public bool LEADmobilephone_IsNull
    {
        set
        {
            if (value)
            {
                SetNull(5);
            }
            else
            {
                throw new InvalidOperationException("IsNull property cannot be set to False. Assign a value to the column instead.");
            }
        }
    }

    public String LEADtelephone2
    {
        set
        {
            this[6] = value;
        }
    }
    public bool LEADtelephone2_IsNull
    {
        set
        {
            if (value)
            {
                SetNull(6);
            }
            else
            {
                throw new InvalidOperationException("IsNull property cannot be set to False. Assign a value to the column instead.");
            }
        }
    }

    public String LEADemailaddress1
    {
        set
        {
            this[7] = value;
        }
    }
    public bool LEADemailaddress1_IsNull
    {
        set
        {
            if (value)
            {
                SetNull(7);
            }
            else
            {
                throw new InvalidOperationException("IsNull property cannot be set to False. Assign a value to the column instead.");
            }
        }
    }

    public String LEADnocrystal
    {
        set
        {
            this[8] = value;
        }
    }
    public bool LEADnocrystal_IsNull
    {
        set
        {
            if (value)
            {
                SetNull(8);
            }
            else
            {
                throw new InvalidOperationException("IsNull property cannot be set to False. Assign a value to the column instead.");
            }
        }
    }

    new public void AddRow()
    {
        base.AddRow();
    }

    new public void SetEndOfRowset()
    {
        base.SetEndOfRowset();
    }

    new public bool EndOfRowset()
    {
        return base.EndOfRowset();
    }

}

public class OutputUgBuffer: ScriptBuffer

{
    public OutputUgBuffer(PipelineBuffer Buffer, int[] BufferColumnIndexes, OutputNameMap OutputMap)
        : base(Buffer, BufferColumnIndexes, OutputMap)
    {
    }

    public String UGmcsname
    {
        set
        {
            this[0] = value;
        }
    }
    public bool UGmcsname_IsNull
    {
        set
        {
            if (value)
            {
                SetNull(0);
            }
            else
            {
                throw new InvalidOperationException("IsNull property cannot be set to False. Assign a value to the column instead.");
            }
        }
    }

    public Guid UGmcsugid
    {
        set
        {
            this[1] = value;
        }
    }
    public bool UGmcsugid_IsNull
    {
        set
        {
            if (value)
            {
                SetNull(1);
            }
            else
            {
                throw new InvalidOperationException("IsNull property cannot be set to False. Assign a value to the column instead.");
            }
        }
    }

    new public void AddRow()
    {
        base.AddRow();
    }

    new public void SetEndOfRowset()
    {
        base.SetEndOfRowset();
    }

    new public bool EndOfRowset()
    {
        return base.EndOfRowset();
    }

}

public class OutputErrorFromAlsBuffer: ScriptBuffer

{
    public OutputErrorFromAlsBuffer(PipelineBuffer Buffer, int[] BufferColumnIndexes, OutputNameMap OutputMap)
        : base(Buffer, BufferColumnIndexes, OutputMap)
    {
    }

    public Guid errorUGid
    {
        set
        {
            this[0] = value;
        }
    }
    public bool errorUGid_IsNull
    {
        set
        {
            if (value)
            {
                SetNull(0);
            }
            else
            {
                throw new InvalidOperationException("IsNull property cannot be set to False. Assign a value to the column instead.");
            }
        }
    }

    public String errordescleadals
    {
        set
        {
            this[1] = value;
        }
    }
    public bool errordescleadals_IsNull
    {
        set
        {
            if (value)
            {
                SetNull(1);
            }
            else
            {
                throw new InvalidOperationException("IsNull property cannot be set to False. Assign a value to the column instead.");
            }
        }
    }

    public String requestcriteria
    {
        set
        {
            this[2] = value;
        }
    }
    public bool requestcriteria_IsNull
    {
        set
        {
            if (value)
            {
                SetNull(2);
            }
            else
            {
                throw new InvalidOperationException("IsNull property cannot be set to False. Assign a value to the column instead.");
            }
        }
    }

    new public void AddRow()
    {
        base.AddRow();
    }

    new public void SetEndOfRowset()
    {
        base.SetEndOfRowset();
    }

    new public bool EndOfRowset()
    {
        return base.EndOfRowset();
    }

}
