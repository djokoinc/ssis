﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Cache;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AlsApi.Model
{
    public class ApiCallContext
    {

        private const string URL_BASE = "platine.actionlogement.fr"; // PROD URL

        public string urlBase { get; set; }

        public string username { get; set; }

        public string password { get; set; }

        public string tokenJWT { get; set; }

        public ApiCallContext(string username, string password, string url)
        {
            if (string.IsNullOrEmpty(username))
            {
                throw new ArgumentException("ApiCallContext constructor : username parameter can't be null or empty", "username");
            }

            if (string.IsNullOrEmpty(password))
            {
                throw new ArgumentException("ApiCallContext constructor : password parameter can't be null or empty", "password");
            }

            if (string.IsNullOrEmpty(url))
            {
                throw new ArgumentException("ApiCallContext constructor : url parameter can't be null or empty", "url");
            }

            this.username = username;
            this.password = password;
            this.urlBase = url;
            this.tokenJWT = GetToken(username, password);
        }

        public ApiCallContext(string username, string password)
        {
            if (string.IsNullOrEmpty(username))
            {
                throw new ArgumentException("ApiCallContext constructor : username parameter can't be null or empty", "username");
            }

            if (string.IsNullOrEmpty(password))
            {
                throw new ArgumentException("ApiCallContext constructor : password parameter can't be null or empty", "password");
            }

            this.username = username;
            this.password = password;
            this.urlBase = URL_BASE;
            this.tokenJWT = GetToken(username, password);
        }

        public string GetToken()
        {
            if (string.IsNullOrEmpty(username)
                || string.IsNullOrEmpty(password))
            {
                throw new Exception("GetToken -> username and password must be set"
                + " before retrieve token, may be try this method :"
                + " GetToken(string username, string password)");
            }

            return (string.IsNullOrEmpty(tokenJWT)) ? GetNewToken() : tokenJWT;
        }

        public string GetToken(string username, string password)
        {
            if (string.IsNullOrEmpty(username))
            { throw new Exception("GetToken username parameter can't be null or empty"); }

            if (string.IsNullOrEmpty(password))
            { throw new Exception("GetToken password parameter can't be null or empty"); }

            this.username = username;
            this.password = password;

            return (string.IsNullOrEmpty(tokenJWT)) ? GetNewToken() : tokenJWT;
        }

        public string GetNewToken()
        {
            if (string.IsNullOrEmpty(username)
                || string.IsNullOrEmpty(password))
            {
                throw new Exception("GetNewToken username and password ApiCallContext properties must be set"
                + " before retrieve token, may be try this method :"
                + " GetToken(string username, string password)");
            }

            HttpWebRequest alsClient = GetWebRequest(Route.AUTH, new LoginCriteria(username, password));
            
            try
            {
                using (HttpWebResponse response = (HttpWebResponse)alsClient.GetResponse())
                {
                    if (response.StatusCode == HttpStatusCode.NoContent
                        && !string.IsNullOrEmpty(response.GetResponseHeader("Authorization")))
                    {
                        return response.GetResponseHeader("Authorization");
                    }
                    if (response.StatusCode == HttpStatusCode.Unauthorized)
                    {
                        throw new Exception("GetToken : Unauthaurization response");
                    }
                    else
                    {
                        throw new Exception(string.Format("Error occured when retrieving token.{0}"
                            + "HttpStatusCode : {1}", Environment.NewLine, response.StatusCode));
                    }
                };
            }
            catch (WebException)
            {
                return null;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public void addNewTokenToRequest(ref HttpWebRequest request)
        {
            tokenJWT = GetNewToken();
            request.Headers.Add(HttpRequestHeader.Authorization, tokenJWT);
        }

        public HttpWebRequest GetWebRequest(string url, int size = 20, int page = 0)
        {
            if (string.IsNullOrEmpty(url))
            {
                throw new ArgumentException("GetWebRequest : url argument can't be empty or null", "url");
            }

            string urlRequest = GetWebRequestUrl(url, size.ToString(), page.ToString());

            HttpWebRequest webReq = WebRequest.CreateHttp(urlRequest);

            if (url != Route.AUTH)
            {
                tokenJWT = GetToken();
                webReq.Headers.Add(HttpRequestHeader.Authorization, tokenJWT);
            }
            
            webReq.ContentType = "application/json";
            webReq.Method = "POST";

            return webReq;
        }

        public HttpWebRequest GetWebRequest(string url, Criteria criteria, int size = 20, int page = 0)
        {
            HttpWebRequest webReq = GetWebRequest(url, size, page);

            // Set Parameter Body
            criteria.FillBody(ref webReq);

            return webReq;
        }

        public string GetWebRequestUrl(string urlController, string size, string page)
        {
            if (string.IsNullOrEmpty(urlController))
            {
                throw new ArgumentException("GetWebRequestUrl : urlController Argument is null or empty", "urlController");
            }

            if (string.IsNullOrEmpty(size))
            {
                throw new ArgumentException("GetWebRequestUrl : size Argument is null or empty", "size");
            }

            if (string.IsNullOrEmpty(page))
            {
                throw new ArgumentException("GetWebRequestUrl : page Argument is null or empty", "page");
            }

            return string.Format("{0}{1}?size={2}&page{3}",
                urlBase,
                urlController,
                size,
                page); 
        }

        public Response GetResponse(HttpWebRequest request, bool mustContinue = true)
        {
            Response res = new Response();
            try
            {

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    using (Stream stream = response.GetResponseStream())
                    using (StreamReader streamReader = new StreamReader(stream))
                    {
                        res = JsonConvert.DeserializeObject<Response>(streamReader.ReadToEnd().ToString());
                    };

                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        throw new Exception(string.Format("Error on CandidatController.GetByDemande"
                            + " --> HttpStatusCode : {0}", response.StatusCode));
                    }

                    return res;
                }
            }
            catch (WebException webE)
            {
                using (HttpWebResponse resp = (HttpWebResponse)webE.Response)
                {
                    if (resp.StatusCode == HttpStatusCode.Unauthorized
                        && mustContinue)
                    {
                        addNewTokenToRequest(ref request);
                        return GetResponse(request, false);
                    }
                    else
                    {
                        throw new Exception(webE.Message);
                    }
                }
            }
            catch(Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        /// <summary>
        /// To complete if there is need to handle TooMany request
        /// </summary>
        /// <param name="webResponse"></param>
        /// <returns></returns>
        private bool IsTooManyWebException(HttpWebResponse webResponse)
        {
            string responseStr = "";
            if(webResponse == null)
            {
                return false;
            }
            Stream stream = webResponse.GetResponseStream();
            if(stream == null)
            {
                return false;
            }
            StreamReader streamReader = new StreamReader(stream);
            if (streamReader == null)
            {
                return false;
            }
            responseStr = streamReader.ReadToEnd();

            return (responseStr.Contains("429") || responseStr.Contains("Too many requests"));
        }

        internal static class Route
        {
            public const string AUTH = "/auth/login";
            public const string LOGEMENT_DEMANDE = "/inli/logements/demandes/_search";
            public const string LOGEMENT_TITULAIRES = "/inli/logements/titulaires/_search";
        }
    }
}
