﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlsApi.Model
{
    public class TitulaireCriteria : Criteria
    {
        public string nom { get; set; }
        public string prenom { get; set; }
        public string dateDeNaissance { get; set; }
        public bool actif { get; set; }
        public bool emailPresent { get; set; }
    }
}
