﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace AlsApi.Model
{
    public class DemandeCriteria : Criteria
    {
        public string codePostal { get; set; }
        public List<string> typologies { get; set; }
        public int compositionFamiliale { get; set; }
        public int ressourcesMensuellesMin { get; set; }
        public bool actif { get; set; }
        public bool emailPresent { get; set; }
        
        /// <summary>
        /// Get typo correspondence from Petrus to ALS 
        /// </summary>
        /// <param name="typo"></param>
        /// <returns></returns>
        public string getTypoCorrespondence(string typo)
        {
            List<string> typologiesALS = new List<string>() { "T1","T2","T3","T4","T5","T6OUPLUS" };

            switch (typo)
            {
                case "Studio":
                    typo = "T1";
                    break;
                case "T6 et +":
                    typo = "T6OUPLUS";
                    break;
                case "T1":
                case "T2":
                case "T3":
                case "T4":
                case "T5":
                    break;
                default:
                    typo = null;
                    break;
            }          
                        
            return typo;
        }
    }
}
