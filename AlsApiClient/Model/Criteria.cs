﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace AlsApi.Model
{
    public class Criteria
    {
        public void FillBody(ref HttpWebRequest request) {
            using (Stream stream = request.GetRequestStream())
            using (StreamWriter streamW = new StreamWriter(stream))
            {
                streamW.Write(JsonConvert.SerializeObject(this, Formatting.Indented));
                streamW.Flush();
                streamW.Close();
            }
        }

        public string ToJSONString()
        {
            try
            {
                return JsonConvert.SerializeObject(this, Formatting.Indented);
            } catch
            {
                return "Criteria.ToJSONString - Error on JSON Formating";
            }            
        }

    }
}
