﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlsApi.Model
{
    class LoginCriteria : Criteria
    {
        
        public string username { get; set; }
        public string password { get; set; }

        public LoginCriteria(string username, string password)
        {
            this.username = username;
            this.password = password;
        }
    }
}
