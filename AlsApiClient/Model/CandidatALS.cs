﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AlsApi.Model
{ 
    public class CandidatALS
    {
        public bool actif { get; set; }
        public string cil { get; set; }
        public int compositionFamiliale { get; set; }
        public string dateDeNaissance { get; set; }
        public List<string> emails { get; set; }
        public string idDossier { get; set; }
        public string libelleStatut { get; set; }
        public List<string> localisations { get; set; }
        public string nom { get; set; }
        public string nomEntreprise { get; set; }
        public string numeroCristal { get; set; }
        public string prenom { get; set; }
        public float ressourcesMensuelles { get; set; }
        public float revenuFiscalNMoins1 { get; set; }
        public float revenuFiscalNMoins2 { get; set; }
        public string siret { get; set; }
        public List<string> telephones { get; set; }
        public List<string> typologies { get; set; }
        public bool validation { get; set; }
        public bool IsOneValidMails()
        {
            bool error = true;
            // Check mails 
            foreach (string email in emails)
            {
                if (IsValidMail(email))
                {
                    error = false;
                }
            }

            return error;
        }

        public static bool IsValidMail(string mail)
        {
            if (string.IsNullOrEmpty(mail))
            {
                return false;
            }

            Regex regEmails = new Regex(@"^[a-zA-Z0-9._-]+?@[a-zA-Z0-9_-]+?\.[a-zA-Z]+$");

            return regEmails.IsMatch(mail); 
        }
    }
}
