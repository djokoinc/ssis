﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlsApi.Model
{
    public class Response
    {
        public List<CandidatALS> content { get; set; }
        public bool first { get; set; }
        public bool last { get; set; }
        public int number { get; set; }
        public int numberOfElements { get; set; }
        public int size { get; set; }
        public Sort sort { get; set; }
        public int totalElements { get; set; }
        public int totalPages { get; set; }
    }
}
