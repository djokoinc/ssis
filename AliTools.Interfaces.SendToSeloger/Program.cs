﻿using System;
using System.Collections.Generic;
using System.IO;

namespace SendToSeLoger
{
    /// <summary>
    /// App console to send folder to SeLoger.com
    /// => get a folder path by config or argument
    /// => check content folder to match Seloger folder 
    ///     Files waiting : Annoces.csv, Photos.cfg, Config.txt
    /// => compress all files in a zip folder
    /// => send all folder to SeLoger.com
    /// </summary>
    class Program
    {
        /// <summary>
        /// Main Program 
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            SeLogerContext context = new SeLogerContext();
            List<string> DirectoryNotSend = new List<string>();
            List<string> DirectorySend = new List<string>();

            // Handle every args passed to the program
            context.init(args);
 
            if (context.verbose)
            {
                Console.WriteLine();
                Console.WriteLine("********* Program SendToSeloger started *********");
                Console.WriteLine();
            }

            // Check context to know if the directory is a unique
            // directory to send or if it contains some directories to send
            if (context.uniqueProcess)
            {
                uniqueProcess(context, ref DirectoryNotSend, ref DirectorySend);
            }
            else
            {
                multipleProcess(context, ref DirectoryNotSend, ref DirectorySend);
            }

            // Get a resume of proccess 
            showResult(context, DirectoryNotSend, DirectorySend);
            // Show error if there is more than 0 and "false" 
            // is there to not exit the program if there is some error
            context.throwErrors(false);

            // If verbose is true, we wwant to keep the console open
            if (context.verbose)
            {
                Console.WriteLine();
                Console.WriteLine("********* Program SendToSeloger finished ********");
                Console.WriteLine("Press a key to exit!");
                Console.ReadKey();
            }
            else
            {
                // If DEBUG is true, we wwant to keep the console open
#if DEBUG
                Console.WriteLine("Press a key to exit!");
                Console.ReadKey();
#endif
            }
        }

        /// <summary>
        /// Use this function when the {{dataFolderPath}} contains some agencies
        /// folder
        /// </summary>
        /// <param name="context"></param>
        /// <param name="DirectoryNotSend"></param>
        /// <param name="DirectorySend"></param>
        internal static void multipleProcess(SeLogerContext context, ref List<string> DirectoryNotSend, ref List<string> DirectorySend)
        {
            DirectoryInfo dirSeloger = new DirectoryInfo(context.dataFolderPath);
            if (!dirSeloger.Exists)
            {
                Console.WriteLine("Argument passed do not represent a existing folder, please pass an existing folder!");
            }

            DirectoryInfo[] agencyDirs = dirSeloger.GetDirectories();
            if (agencyDirs.Length <= 0)
            {
                Console.WriteLine("Directory passed do not contain any agency directory.");
            }

            foreach (DirectoryInfo agencyDir in agencyDirs)
            {
                bool isFolderSend = false;
                if (SeLogerFolder.isSeLogerDir(agencyDir, SeLogerFolder.mandatoryFileName, true))
                {
                    SeLogerProccess agencyProcccess = new SeLogerProccess(agencyDir.FullName);
                    try
                    {
                        isFolderSend = agencyProcccess.proccessing(context);
                        if (!isFolderSend)
                        {
                            context.errors.Add(SeLogerProccess.ErrorMesssage.sendError(context, agencyDir.FullName, string.Join(Environment.NewLine, agencyProcccess.errors)));
                        }

                    } catch( Exception e)
                    {
                        // Handle exception
                        context.errors.Add(SeLogerProccess.ErrorMesssage.sendError(context, agencyDir.FullName, e.Message));
                    }
                }

                if (!isFolderSend)
                {
                    DirectoryNotSend.Add(string.Format("Directory : {0}.",
                        agencyDir.FullName));
                } else
                {
                    DirectorySend.Add(string.Format("Directory : {0}.",
                        agencyDir.FullName));
                }
            }
        }

        /// <summary>
        /// Use this function if the {{dataFolderPath}} represent 
        /// a unique agency folder
        /// </summary>
        /// <param name="context"></param>
        /// <param name="DirectoryNotSend"></param>
        /// <param name="DirectorySend"></param>
        internal static void uniqueProcess(SeLogerContext context, ref List<string> DirectoryNotSend, ref List<string> DirectorySend)
        {
            DirectoryInfo agencyDir = new DirectoryInfo(context.dataFolderPath);
            if (!agencyDir.Exists)
            {
                Console.WriteLine("Argument passed do not represent a existing folder, please pass an existing folder!");
            }

            bool isFolderSend = false;

            if (SeLogerFolder.isSeLogerDir(agencyDir, SeLogerFolder.mandatoryFileName, true))
            {                
                SeLogerProccess agencyProcccess = new SeLogerProccess(agencyDir.FullName);
                try {
                    isFolderSend = agencyProcccess.proccessing(context);
                    if (!isFolderSend)
                    { 
                        context.errors.Add(SeLogerProccess.ErrorMesssage.sendError(context, agencyDir.FullName, string.Join(Environment.NewLine, agencyProcccess.errors)));
                    }
                }
                catch (Exception e)
                {
                    // Handle exception
                    context.errors.Add(SeLogerProccess.ErrorMesssage.sendError(context, agencyDir.FullName, e.Message));
                }
            }

            if (!isFolderSend)
            {
                DirectoryNotSend.Add(string.Format("Directory : {0}.",
                    agencyDir.FullName));
            } else
            {
                DirectorySend.Add(string.Format("Directory : {0}.",
                    agencyDir.FullName));
            }
        }        

        /// <summary>
        /// Use this function to get a resume of the execution
        /// and to say wich folder has been send and which wasn't
        /// </summary>
        /// <param name="context"></param>
        /// <param name="DirectoryNotSend"></param>
        /// <param name="DirectorySend"></param>
        internal static void showResult(SeLogerContext context, List<string> DirectoryNotSend, List<string> DirectorySend)
        {
            string resume = string.Format("========================================================={0}"
                + "===========> Proccess SendToSeLoger - result <==========={0}"
                + "===> Folder send {0}{1}{0}"
                + "===> Folder not send {0}{2}{0}",
                Environment.NewLine,
                string.Join(Environment.NewLine, DirectorySend),
                string.Join(Environment.NewLine, DirectoryNotSend)
                );

            Log.trace(resume);

            if (!context.verbose)
            {
                Console.WriteLine("==================== Proccess Resume ===================="
                    + "{0}{1}{0}"
                    + "==================== Proccess Resume end ================",
                    Environment.NewLine,
                    resume);
            }
        }
    }
}
