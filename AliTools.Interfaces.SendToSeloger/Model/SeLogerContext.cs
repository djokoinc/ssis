﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text.RegularExpressions;

namespace SendToSeLoger
{
    /// <summary>
    /// Class equivalent of the execution context of the program, 
    /// will get every properties by arguments or from app.config 
    /// </summary>
    public class SeLogerContext
    {
        #region property 
        public const string ERROR_START = "============================ ERRORS ============================";

        public List<string> errors { get; set; }

        public bool uniqueProcess { get; set; } = false;

        public bool verbose { get; set; } = false;

        public string ftpProtocoleSuffix { get; set; } = "ftp://";

        public string ftpHost { get; set; }

        public string ftpPort { get; set; }

        public string ftpUser { get; set; }

        public string ftpPassword { get; set; }

        /// <summary>
        /// Config properties
        /// </summary>
        #region Property From Config
        public string dataFolderPath;
        public string archiveFolderPath;
        public string logFolderPath;
        public string tmpFolderName;
        public string tmpFolderPath;
        public string versionSpecification;
        public string versionAppInli;
        public string nameAppInli;
        private string ftpStringConnection;        
        #endregion

        #endregion

        #region Constructeur

        public SeLogerContext()
        {
            errors = new List<string>();
        }

        #endregion

        // Function to call to Init 
        public void init(string[] args)
        {
            // Get config parameter from App file
            initConfig();

            // Handle arguments which can change config value
            handleArgs(args);

            // Init Tmp archive folder
            initTmpArchiveFolder();

            // Throw Error and exit the program if there is one 
            throwErrors();
        }

        /// <summary>
        /// Retrieve every config from app.config
        /// </summary>
        public void initConfig()
        {
            getConfOrFillError("DataFolderPath", out dataFolderPath);
            getConfOrFillError("ArchiveFolderPath", out archiveFolderPath);
            getConfOrFillError("TMPArchiveFolderName", out tmpFolderName);
            getConfOrFillError("VersionSpecificationSeLoger", out versionSpecification);
            getConfOrFillError("VersionAppInli", out versionAppInli);
            getConfOrFillError("NameAppInli", out nameAppInli);         
            getConfOrFillError("LogFolderPath", out logFolderPath);

            string ftpHostConf;
            getConfOrFillError("FTPHost", out ftpHostConf);
            ftpHost = ftpHostConf;

            string ftpUserConf;
            getConfOrFillError("FTPUser", out ftpUserConf);
            ftpUser = ftpUserConf;

            string ftpPasswordConf;
            getConfOrFillError("FTPPwd", out ftpPasswordConf);
            ftpPassword = ftpPasswordConf;

            string ftpPortConf;
            getConfOrFillError("FTPPort", out ftpPortConf);
            ftpPort = ftpPortConf;
        }

        public void getConfOrFillError(string confName, out string confValue)
        {
            if (!tryGetSetting(confName, out confValue))
            {
                errors.Add("Config Error => "+ confName);
            }
        }

        /// <summary>
        /// Function to retrieve a config value
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        private static bool tryGetSetting(string key, out string value)
        {
            value = ConfigurationManager.AppSettings[key];

            if ((string.IsNullOrEmpty(value) || string.IsNullOrWhiteSpace(value)))
            {
                Console.WriteLine("Setting Error => Name : {0}",
                    key);
            }

            return !(string.IsNullOrEmpty(value) || string.IsNullOrWhiteSpace(value));
        }

        #region Handle Program Arguments 

        /// <summary>
        /// Handle args and handle help parameter 
        /// </summary>
        /// <param name="args"></param>
        internal void handleArgs(string[] args)
        {
            if (args.Length > 0)
            {
                if (args[0] == "--help" || args[0] == "-h" || args[0] == "help" || args[0] == "/?")
                {
                    helps();
                }

                if (!(args[0].StartsWith("-") || args[0].StartsWith("--")))
                {
                    throw new ArgumentException("Program Argument must begin by - or --");
                }

                for (int i = 0; i < args.Length; i++)
                {
                    string arg = args[i];

                    if (!(arg.StartsWith("-") || arg.StartsWith("--")))
                    {
                        continue;
                    }

                    handleArgValue(arg, args, i);
                }
            }
        }

        /// <summary>
        /// Handle a argument from a param rise from handleArgs function
        /// </summary>
        /// <param name="arg"></param>
        /// <param name="args"></param>
        /// <param name="indexArg"></param>
        private void handleArgValue(string arg, string[] args, int indexArg)
        {
            if (arg == "-u" || arg == "--unique")
            {
                uniqueProcess = true;
            }
            else if (arg == "-v" || arg == "--verbose")
            {
                verbose = true;
            }
            else if (arg == "-s" || arg == "--source")
            {
                tryGetArgsValue(indexArg, args, ref dataFolderPath);
            }
            else if (arg == "-us" || arg == "--uniquesource")
            {
                uniqueProcess = true;
                tryGetArgsValue(indexArg, args, ref dataFolderPath);
            }
            else if (arg == "-a" || arg == "--archive")
            {
                tryGetArgsValue(indexArg, args, ref archiveFolderPath);
            }
            else if (arg == "-l" || arg == "--log")
            {
                tryGetArgsValue(indexArg, args, ref logFolderPath);
            }
            else if (arg == "-c" || arg == "--connection")
            {
                tryGetArgsValue(indexArg, args, ref ftpStringConnection);
                handleFtpConnection(ftpStringConnection);
            }
        }

        private void handleFtpConnection(string connectionStr)
        {
            // Init Sample sentence
            string connectionStrSample = "ftpHost='ftp.exemple.com';ftpPort='21';"
                + "ftpUser='USER';ftpPassword='MOT DE PASSE';";
            string connectionValueSample = "ftpUser='USER'";
            // Regex to catch property and value in connection String
            Regex catchArg = new Regex(@"(\w+)?=('.*?');");
            // All property we can affect with the connection string 
            List<string> acceptedProperties = new List<string> { "ftpHost", "ftpPort", "ftpUser", "ftpPassword" };
            Dictionary<string, string> propertiesToFill = new Dictionary<string, string>();
            int errorCount = 0;
            // Apply regex to connection string
            MatchCollection res = catchArg.Matches(connectionStr);

            if (res == null || res.Count < 1)
            {
                errors.Add(string.Format("Ftp Connection String couldn't be parse.{0}"
                    + " - given value : \"{1}\"{0} - format expected : \"{2}\"",
                    Environment.NewLine,
                    connectionStr,
                    connectionStrSample));
                return;
            }

            foreach (Match match in res)
            {
                if (match.Groups == null || match.Groups.Count != 3)
                {
                    errors.Add(string.Format("Ftp Connection String couldn't parse a matched value.{0}"
                        + "- given value : \"{1}\"{0} - format expected : \"{2}\"",
                    Environment.NewLine,
                    match.Value,
                    connectionValueSample));
                    errorCount++;
                    continue;
                }

                if (!acceptedProperties.Contains(match.Groups[1].Value))
                {
                    errors.Add(string.Format("Ftp Connection String : given value didn't match any property.{0}"
                        + "- given property value : \"{1}\"{0} - property accepted : \"{2}\"",
                    Environment.NewLine,
                    match.Groups[1].Value,
                    string.Join(",", acceptedProperties)));
                    errorCount++;
                    continue;
                }

                string varName = match.Groups[1].Value;
                string varValue = match.Groups[2].Value?.Replace("'","");

                propertiesToFill.Add(varName, varValue);
            }

            if (errorCount == 0)
            {
                fillProperties(propertiesToFill);
            }
        }

        private void fillProperties(Dictionary<string, string> properties)
        {
            if (properties == null || properties.Count < 1)
            {
                return;
            }

            foreach (KeyValuePair<string, string> prop in properties)
            {
                GetType()?.GetProperty(prop.Key, BindingFlags.Instance |
                    BindingFlags.Public | BindingFlags.Public )?.SetValue(this, prop.Value);
            }
        }

        private void tryGetArgsValue(int index, string[] args, ref string param)
        {

            if (index + 1 > args.Length)
            {
                errors.Add(string.Format("Argument {0} need a value", args[index]));
            }
            else
            {
                if (string.IsNullOrWhiteSpace(args[index + 1]))
                {
                    errors.Add(string.Format("Argument {0} can't be empty or white space", args[index]));
                    return;
                }
                param = args[index + 1];
            }
        }

        #endregion
        /// <summary>
        /// Initialyze the temp folder use by the program to process each folder to send
        /// </summary>
        public void initTmpArchiveFolder()
        {
            DirectoryInfo TMPDir;
            string exePath = AppDomain.CurrentDomain.BaseDirectory;
            DirectoryInfo dirExe = new DirectoryInfo(exePath);
            DirectoryInfo[] dirsTMP = dirExe.GetDirectories(tmpFolderName);

            if (dirsTMP.Count() < 1)
            {
                TMPDir = dirExe.CreateSubdirectory(tmpFolderName);
            }
            else
            {
                TMPDir = dirsTMP[0];
            }

            tmpFolderPath = TMPDir.FullName;
        }

        /// <summary>
        /// Get a FtpWebRequest configure by context
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public FtpWebRequest getFtpUpLoadRequest(string fileName)
        {            
            if (string.IsNullOrWhiteSpace(ftpHost)
                || string.IsNullOrWhiteSpace(ftpUser)
                || string.IsNullOrWhiteSpace(ftpPassword))
            {
                throw new Exception("SendToSeLoger : SFTP parameters can't be empty");
            }
            string uriFtp = ftpProtocoleSuffix;
            uriFtp += ftpHost.EndsWith(@"/") ? ftpHost + fileName : ftpHost + @"/" + fileName;
            
            FtpWebRequest ftpClient = (FtpWebRequest)WebRequest.Create(uriFtp);
            ftpClient.Method = WebRequestMethods.Ftp.UploadFile;
            ftpClient.UseBinary = true;
            ftpClient.KeepAlive = false;
            ftpClient.Credentials = new NetworkCredential(ftpUser, ftpPassword);
            
            return ftpClient;
        }
        
        /// <summary>
        /// Get a string representation of the context
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string formatTxt = "SeLogerContext :"
                + "{0}dataFolderPath : {1}"
                + "{0}archiveFolderPath : {2}"
                + "{0}tmpDirectoryPath : {3}"
                + "{0}logFolderPath : {4}"
                + "{0}Errors :{0}{5}";


            return string.Format(formatTxt,
                Environment.NewLine,
                dataFolderPath,
                archiveFolderPath,
                tmpFolderPath,
                logFolderPath,
                toErrorString());
        }

        /// <summary>
        /// Show every error catch form the context
        /// </summary>
        /// <returns></returns>
        public string toErrorString()
        {
            return string.Join(Environment.NewLine, errors);
        }

        /// <summary>
        /// Throw error if there is at least one
        /// {{mustExit}} is set to true if we need to 
        /// exit the program with an error if there is one
        /// </summary>
        /// <param name="mustExit"></param>
        public void throwErrors(bool mustExit = true)
        {
            if (errors.Count > 0)
            {
                Console.WriteLine("{2}{1}Execution errors : {1}{0}",
                    this.toErrorString(),
                    Environment.NewLine,
                    ERROR_START);

                if (mustExit)
                {
                    Environment.Exit(1);
                } else
                {
                    Environment.ExitCode = 1;
                }                
            }
        }

        /// <summary>
        /// Return "help"
        /// Representation of each argument handle by
        /// the program
        /// </summary>
        public void helps()
        {
            string helps = "===================> SendToSeLoger.exe {0}{0}"
                + "Options :{0}"
                + "    --verbose  || -v {0}"
                + "        Use this to get all log in console{0}"
                + "    --unique   || -u {0}"
                + "        Use to tell that the directory Seloger given by argument or"
                + " config is the directory to send. Use this option to send a unique directory,"
                + "for example to send again a agency directory{0}"
                + "    --source   || -s <Path to Seloger directory> {0}"
                + "        Use option (-u || --unique) if the directory given is an agency directory (with mandatory file to send) {0}"
                + "    --archvive || -a <Path to Archive directory> {0}"
                + "        Use to precise a new Archive folder.{0}"
                + "    --log || -l <Path to Log directory> {0}"
                + "        Use to precise a new Log folder.{0}"
                + "    --connection || -c <String connection for FTP> {0}"
                + "        A string connection use to fill FTP property.{0}"
                + "        Exemple : \"ftpHost='ftp.exemple.com';ftpPort='22';"
                + "ftpUser='USER';ftpPassword='MOT DE PASSE';\"";

            Console.WriteLine(helps, Environment.NewLine);
            Environment.Exit(0);
        }
    }
}
