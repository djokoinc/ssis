﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace SendToSeLoger
{
    /// <summary>
    /// Class Singleton for Log 
    /// </summary>
    public sealed class Log
    {
        /// <summary>
        /// unique instance of Log
        /// </summary>
        private static readonly Log instance = new Log();

        /// <summary>
        /// Get the unique instance of Log
        /// </summary>
        public static Log Instance
        {
            get
            {
                return instance;
            }
        }

        /// <summary>
        /// Content of the log instance since his creation 
        /// </summary>
        public List<string> logContents { get; set; }

        /// <summary>
        /// Path to log file 
        /// </summary>
        public string logFilePath { get; set; }

        /// <summary>
        /// Name of the log file
        /// </summary>
        public string logFileName { get; set; }

        /// <summary>
        /// date format use for log
        /// </summary>
        public string dateFormat { get; set; }

        /// <summary>
        /// Method use to log
        /// </summary>
        public Out outMethod { get; set; } = Out.Console;

        /// <summary>
        /// Enum of possible out for log
        /// </summary>
        public enum Out
        {
            File,
            Console
        }

        /// <summary>
        /// Static function to overload constructor
        /// </summary>
        static Log()
        {
        }

        /// <summary>
        /// Private constructor use to instanciate once instance
        /// </summary>
        private Log()
        {
            dateFormat = "dd-MM-yyyy HH:mm:ss.fff";
            logFileName = Assembly.GetExecutingAssembly().GetName().Name + ".log";
            logContents = new List<string>();
            
            DirectoryInfo dirExe = new DirectoryInfo(Environment.CurrentDirectory);

            logFilePath = dirExe.FullName.EndsWith(@"\") ? 
                dirExe.FullName + logFileName 
                : dirExe.FullName + @"\" + logFileName;

        }

        /// <summary>
        /// Set Out method for the instance (File or Console)
        /// </summary>
        /// <param name="outMethodParam"></param>
        /// <param name="filePath"></param>
        public static void setOutMethod(Out outMethodParam, string filePath = null)
        {
            instance.outMethod = outMethodParam;

            if(outMethodParam == Out.File)
            {
                instance.initFile(filePath);
            }
        }

        /// <summary>
        /// Initialyzation of the log file use to log everythings.
        /// </summary>
        /// <param name="filePath"></param>
        private void initFile(string filePath)
        {
            if (string.IsNullOrWhiteSpace(filePath))
            {
                if (string.IsNullOrWhiteSpace(logFilePath))
                {
                    throw new Exception("Log : filePath argument is null and we couldn't create log file with defaut path");
                }
                filePath = logFilePath;
            } 
            
            try
            {
                if (!File.Exists(filePath))
                {
                    using (FileStream fs = File.Create(filePath))
                    {
                        string createdLineStr = string.Format(
                              "**** Auto-generated file from SendToSeloger.exe {0}"
                            + "**** Created : {1}{0}"
                            + "**** Administrator : Johan Dumouchel (johan.dumouchel@astrimmo.com){0}",
                            Environment.NewLine,
                            DateTime.Now.ToString("G"));

                        Byte[] createdLine = new UTF8Encoding(true).GetBytes(createdLineStr);
                        fs.Write(createdLine, 0, createdLine.Length);
                    };                    
                }                
                logFilePath = filePath;
            }
            catch(Exception e)
            {
                throw new Exception(
                    string.Format("Log file '{0}' couldn't be created - Exception.Message = '{1}'",
                    filePath,
                    e.Message));
            }
        }

        /// <summary>
        /// Private method call to add a log line on the log, 
        /// the out method will defined where put the line
        /// </summary>
        /// <param name="logLine"></param>
        private void add(string logLine)
        {
            logContents.Add(logLine);

            if(instance.outMethod == Out.Console)
            {
                Console.WriteLine(logLine);
            }

            if (instance.outMethod == Out.File && File.Exists(logFilePath))
            {
                using (StreamWriter writer = File.AppendText(logFilePath))
                {
                    writer.WriteLine(logLine);
                }

            }
        }

        /// <summary>
        /// Static method to call to log a line inside 
        /// the log file or the console
        /// </summary>
        /// <param name="logTxt"></param>
        public static void trace(string logTxt)
        {
            instance.add(logTxt);
        }
    }
}
