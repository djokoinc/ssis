﻿using System;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;

namespace SendToSeLoger
{
    /// <summary>
    /// Class use to instanciate a folder 
    /// as expected from Seloger.com
    /// </summary>
    public class SeLogerFolder
    {
        /// <summary>
        /// instance of the folder
        /// </summary>
        public DirectoryInfo sourceFolder { get; set; }
        /// <summary>
        /// All file require by Seloger.com
        /// </summary>
        public static string extensionCompressFile = ".zip";
        public static string mandatoryFileName = "Annonces.csv";
        public static string configFileName = "Config.txt";
        public static string photosConfigFileName = "Photos.cfg";

        /// <summary>
        /// Constructor 
        /// </summary>
        /// <param name="folderName"></param>
        public SeLogerFolder(string folderName)
        {
            sourceFolder = new DirectoryInfo(folderName);

            if (!sourceFolder.Exists)
                throw new Exception("SeLogerFolder => folder not found, path : " + folderName);
        }

        /// <summary>
        /// Use to copy a directory 
        /// </summary>
        /// <param name="destinationPath"></param>
        /// <returns></returns>
        public DirectoryInfo copyTo(string destinationPath, SeLogerContext context)
        {
            if(string.IsNullOrWhiteSpace( destinationPath ) 
                || !Directory.Exists( destinationPath ) )
            {
                return null;
            }

            if (!sourceFolder.Exists)
            {
                return null;
            }

            destinationPath = (destinationPath.EndsWith(@"\") ? destinationPath : destinationPath + @"\") +
                 context.nameAppInli + "_" + sourceFolder.Name;
            
            if (copyFolderContents(this.sourceFolder.FullName, destinationPath) && Directory.Exists(destinationPath))
            {
                return new DirectoryInfo(destinationPath);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Use to copy every files on a directory to an other directory
        /// </summary>
        /// <param name="sourcePath"></param>
        /// <param name="destinationPath"></param>
        /// <returns></returns>
        public static bool copyFolderContents(string sourcePath, string destinationPath)
        {
            sourcePath = sourcePath.EndsWith(@"\") ? sourcePath : sourcePath + @"\";
            destinationPath = destinationPath.EndsWith(@"\") ? destinationPath : destinationPath + @"\";

            try
            {
                if (Directory.Exists(sourcePath))
                {
                    if (Directory.Exists(destinationPath) == false)
                    {
                        Directory.CreateDirectory(destinationPath);
                    }

                    foreach (string files in Directory.GetFiles(sourcePath))
                    {
                        FileInfo fileInfo = new FileInfo(files);
                        fileInfo.CopyTo(string.Format(@"{0}\{1}", destinationPath, fileInfo.Name), true);
                    }

                    foreach (string drs in Directory.GetDirectories(sourcePath))
                    {
                        DirectoryInfo directoryInfo = new DirectoryInfo(drs);
                        if (copyFolderContents(drs, destinationPath + directoryInfo.Name) == false)
                        {
                            return false;
                        }
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Use to add every required file to the folder
        /// sourceFolder
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public bool addFiles(SeLogerContext context)
        {
            return (addConfigFile(context) && addPhotosConfigFile(context)) ;
        }

        /// <summary>
        /// Use to add a file call Config.txt
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public bool addConfigFile(SeLogerContext context)
        {
            try
            {
                string configPath = sourceFolder.FullName.EndsWith(@"\") ? sourceFolder.FullName + configFileName : sourceFolder.FullName + @"\" + configFileName;
                // Create a file to write to.
                using (StreamWriter sw = File.CreateText(configPath))
                {
                    sw.WriteLine(
                        string.Format("Version={0}",
                        context.versionSpecification));

                    sw.WriteLine(
                        string.Format("Application={0}_{1}",
                        context.nameAppInli,
                        context.versionAppInli));

                    sw.WriteLine("Devise=Euro");
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// USe to add phto.cgl file to sourceFolder
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public bool addPhotosConfigFile(SeLogerContext context)
        {
            try
            {
                string photosConfigPath = sourceFolder.FullName.EndsWith(@"\") ? sourceFolder.FullName + photosConfigFileName : sourceFolder.FullName + @"\" + photosConfigFileName;
                // Create a file to write to.
                using (FileStream fs = File.Create(photosConfigPath))
                {
                    Byte[] text = new UTF8Encoding(true).GetBytes("Mode=URL");
                    // Add some information to the file.
                    fs.Write(text, 0, text.Length);
                }

                return true;
            }
            catch 
            {
                return false;
            }
        }

        /// <summary>
        /// return a zip folder of sourceFolder
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public string getCompressFolder(SeLogerContext context)
        {
            if (!Directory.Exists(sourceFolder.FullName))
            {
                context.errors.Add(SeLogerProccess.ErrorMesssage.createDirError(sourceFolder.FullName));
            }

            string zipPath = sourceFolder.FullName + extensionCompressFile;

            ZipFile.CreateFromDirectory(sourceFolder.FullName, zipPath);

            return File.Exists(zipPath) ? zipPath : null ;
        }

        /// <summary>
        /// Use to check if the folder given to sourceFolder
        /// is a valid directory or not.
        /// valid = contains every required files
        /// </summary>
        /// <param name="withMultipleLine"></param>
        /// <returns></returns>
        public bool isValidDir(bool withMultipleLine = false)
        {
            if (string.IsNullOrEmpty(mandatoryFileName))
            {
                throw new Exception("IsSeLogerDir -> MandatoryFileName can't be null or empty");
            }

            if (!sourceFolder.Exists)
            {
                throw new Exception("IsSeLogerDir -> sourceFolder must exist");
            }

            return isSeLogerDir(sourceFolder, mandatoryFileName, withMultipleLine);
        }

        /// <summary>
        /// Use to check if the folder given
        /// is a valid directory or not.
        /// valid = contains every required files
        /// </summary>
        /// <param name="folder"></param>
        /// <param name="mandatoryFileName"></param>
        /// <param name="withMultipleLine"></param>
        /// <returns></returns>
        public static bool isSeLogerDir(DirectoryInfo folder, string mandatoryFileName, bool withMultipleLine = false)
        {
            if (string.IsNullOrEmpty(mandatoryFileName))
            {
                throw new Exception("Setting -> MandatoryFileName can't be null or empty");
            }

            if (!folder.Exists)
            {
                return false;
            }

            FileInfo[] files = folder.GetFiles(mandatoryFileName);

            if (files.Length == 1)
            {
                if (withMultipleLine)
                {
                    int lines = File.ReadLines(files[0].FullName).Count();
                    return (lines >= 1);
                }
                else
                {
                    return true;
                }
            }

            return false;
        }

    }
}
