﻿using ALIT.Model;
using ALITools.ALIT.PaW.Plugins.Model;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace ALITools.ALIT.PaW.Plugins.Helper
{
    public static class CrmHelper
    {
        /// <summary>
        /// Gets the initiating user email.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="service">The service.</param>
        /// <returns>The user email</returns>
        public static string GetInitiatingUserEmail(IOrganizationService service, Guid userId)
        {
            if (userId == Guid.Empty)
            {
                throw new InvalidPluginExecutionException(
                    "Une erreur est survenue lors de la recherche de l'email de l'utilisateur courant : "
                    + "l'identifiant de l'utilisateur est vide.");
            }

            var request = new RetrieveRequest();
            request.ColumnSet = new ColumnSet(new string[] { SystemUser.Fields.InternalEMailAddress });
            request.Target = new EntityReference(SystemUser.EntityLogicalName, userId);

            var entity = ((RetrieveResponse)service.Execute(request)).Entity;
            return entity.GetAttributeValue<string>(SystemUser.Fields.InternalEMailAddress);
        }

        /// <summary>
        /// Gets the configuration value.
        /// </summary>
        /// <param name="adminService">The service.</param>
        /// <param name="key">The key.</param>
        /// <param name="valueField">The value field.</param>
        /// <returns></returns>
        /// <exception cref="InvalidPluginExecutionException">
        /// An error occured while retrieving configuration record: "
        ///                     + "the configuration record with key '" + key + "' was not found.
        /// or
        /// An error occured while retrieving configuration record: "
        ///                    + "multiple occurences of the configuration record with key '" + key + "' was found.
        /// </exception>
        public static string GetConfiguration(IOrganizationService adminService, string key, string valueField)
        {
            if (string.IsNullOrEmpty(key))
            {
                throw new InvalidPluginExecutionException(
                    "Une erreur est survenue lors de la recherche de l'élément de configuration : "
                    + "la clef de configuration est vide.");
            }

            if (string.IsNullOrEmpty(valueField))
            {
                throw new InvalidPluginExecutionException(
                    "Une erreur est survenue lors de la recherche de l'élément de configuration : "
                    + "la valeur recherchée est vide.");
            }

            var query = new QueryExpression()
            {
                EntityName = mcs_configuration.EntityLogicalName,
                ColumnSet = new ColumnSet(mcs_configuration.Fields.mcs_name, valueField),
                Criteria = new FilterExpression()
            };
            query.Criteria.AddCondition(mcs_configuration.Fields.mcs_name, ConditionOperator.Equal, key);
            var entities = adminService.RetrieveMultiple(query).Entities;

            if (entities.Count == 0)
            {
                throw new InvalidPluginExecutionException(
                    "Une erreur est survenue lors de la lecture de la configuration : "
                    + "l'enregistrement de configuration avec la clef '" + key + "' n'a pas été trouvée.");
            }

            if (entities.Count > 1)
            {
                throw new InvalidPluginExecutionException(
                   "Une erreur est survenue lors de la lecture de la configuration : "
                   + "plusieurs occurences de la configuration avec la clef '" + key + "' ont été trouvées.");
            }

            var value = entities[0].GetAttributeValue<string>(valueField);
            if (string.IsNullOrEmpty(value))
            {
                throw new InvalidPluginExecutionException("Une erreur est survenue lors de la lecture de l'élément de configuration '" + key + "' : "
                    + "la valeur n'est pas renseignée ou vous n'avez pas les privilèges suffisant pour accèder à cette information.");
            }
            return value;
        }

        /// <summary>
        /// Retrieves the pw document location.
        /// </summary>
        /// <param name="adminService">The service.</param>
        /// <param name="entityLogicalName">Name of the entity logical.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <returns></returns>
        /// <exception cref="InvalidPluginExecutionException">
        /// An error occured while retrieving Plug & Work document location: "
        ///                         + "Entity Logical Name is null or empty.
        /// or
        /// An error occured while retrieving Plug & Work document location: "
        ///                         + "Entity ID is null or empty.
        /// or
        /// An error occured while retrieving Plug & Work document location record: "
        ///                    + "multiple occurences were found for record id '" + entityId + "' was found.
        /// </exception>
        public static string RetrievePWDocumentLocation(
            IOrganizationService adminService, string entityLogicalName, string entityId)
        {
            if (string.IsNullOrEmpty(entityLogicalName))
            {
                throw new InvalidPluginExecutionException(
                    "Une erreur est survenue lors de la recherche de l'espace Plug & Work : "
                    + "le nom logique de l'entité est vide.");
            }

            if (string.IsNullOrEmpty(entityId))
            {
                throw new InvalidPluginExecutionException(
                    "Une erreur est survenue lors de la recherche de l'espace Plug & Work : "
                    + "l'identifiant de l'enregistrement Dynamics 365 est vide.");
            }

            var optionSetEnum =
                (mcs_pawdocumentlocation.Enums.mcs_entitylogicalnamecode)Enum.Parse(
                    typeof(mcs_pawdocumentlocation.Enums.mcs_entitylogicalnamecode),
                    entityLogicalName,
                    true);

            var query = new QueryExpression()
            {
                EntityName = mcs_pawdocumentlocation.EntityLogicalName,
                ColumnSet = new ColumnSet(mcs_pawdocumentlocation.Fields.mcs_pwspaceid),
                Criteria = new FilterExpression()
            };
            query.Criteria.AddCondition(
                mcs_pawdocumentlocation.Fields.mcs_idenregistrement,
                ConditionOperator.Equal,
                entityId);
            query.Criteria.AddCondition(
                mcs_pawdocumentlocation.Fields.mcs_entitylogicalnamecode,
                ConditionOperator.Equal,
                (int)optionSetEnum);
            var entities = ((EntityCollection)adminService.RetrieveMultiple(query)).Entities;

            if (entities.Count == 0)
            {
                return string.Empty;
            }

            if (entities.Count > 1)
            {
                throw new InvalidPluginExecutionException(
                   "Une erreur est survenue lors de la recherche de l'espace documentaire : "
                   + "plusieurs occurences de l'espace ont été trouvées pour l'identifiant '" + entityId + "'.");
            }
            return entities[0].GetAttributeValue<string>(mcs_pawdocumentlocation.Fields.mcs_pwspaceid);
        }

        /// <summary>
        /// Creates the pw document location.
        /// </summary>
        /// <param name="adminService">The service.</param>
        /// <param name="entityLogicalName">Name of the entity logical.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="spaceId">The space identifier.</param>
        /// <exception cref="InvalidPluginExecutionException">
        /// An error occured while creating Plug & Work document location: "
        ///                         + "Entity Logical Name is null or empty.
        /// or
        /// An error occured while creating Plug & Work document location: "
        ///                         + "Entity ID is null or empty.
        /// or
        /// An error occured while creating Plug & Work document location: "
        ///                         + "Space ID is null or empty.
        /// </exception>
        public static Guid CreatePWDocumentLocation(
            IOrganizationService adminService, string name, string entityLogicalName, string entityId, string spaceId)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new InvalidPluginExecutionException(
                    "Une erreur est survenue lors de la création de l'espace Plug & Work : "
                    + "le nom de l'espace est vide.");
            }

            if (string.IsNullOrEmpty(entityLogicalName))
            {
                throw new InvalidPluginExecutionException(
                    "Une erreur est survenue lors de la création de l'espace Plug & Work : "
                    + "le nom logique de l'entité est vide.");
            }

            if (string.IsNullOrEmpty(entityId))
            {
                throw new InvalidPluginExecutionException(
                    "Une erreur est survenue lors de la création de l'espace Plug & Work : "
                    + "l'identifiant de l'enregistrement Dynamics 365 est vide.");
            }

            if (string.IsNullOrEmpty(spaceId))
            {
                throw new InvalidPluginExecutionException(
                    "Une erreur est survenue lors de la création de l'espace Plug & Work : "
                    + "l'identifiant de l'espace documentaire Plug & Work est vide.");
            }

            var logicalNameCode = (mcs_pawdocumentlocation.Enums.mcs_entitylogicalnamecode)Enum.Parse(
                typeof(mcs_pawdocumentlocation.Enums.mcs_entitylogicalnamecode),
                entityLogicalName);

            var location = new Entity(mcs_pawdocumentlocation.EntityLogicalName);

            location[mcs_pawdocumentlocation.Fields.mcs_name] = name;
            location[mcs_pawdocumentlocation.Fields.mcs_entitylogicalnamecode] = new OptionSetValue((int)logicalNameCode);
            location[mcs_pawdocumentlocation.Fields.mcs_idenregistrement] = entityId;
            location[mcs_pawdocumentlocation.Fields.mcs_pwspaceid] = spaceId;

            return adminService.Create(location);
        }

        /// <summary>
        /// Retrieves the Plug & Work authentication token.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <returns>Plug & Work authentication token</returns>
        /// <exception cref="InvalidPluginExecutionException">An error occured while retrieving Plug & Work user token: "
        ///                    + "Web service returns an empty result: " + pwResponse.info</exception>
        public static PawAuthResponse RetrievePWToken(IOrganizationService service)
        {
            var request = new OrganizationRequest("mcs_RetrievePAWToken");
            var response = service.Execute(request);

            if (response.Results.Contains("PAWResponse"))
            {
                var jsonStr = response.Results["PAWResponse"].ToString();
                var pwResponse = JsonConvert.DeserializeObject<PawAuthResponse>(jsonStr);

                if (!string.Equals(pwResponse.Success, "true")
                    || string.IsNullOrEmpty(pwResponse.UserToken))
                {
                    throw new InvalidPluginExecutionException(
                        "Une erreur est survenue lors de la récupération du jeton d'authentification : " + pwResponse.Info);
                }
                return pwResponse;
            }
            return null;
        }

        /// <summary>
        /// Gets the operation.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="operationId">The operation identifier.</param>
        /// <returns>The operation</returns>
        public static Entity GetOperation(IOrganizationService service, Guid operationId)
        {
            if (operationId == Guid.Empty)
            {
                throw new InvalidPluginExecutionException(
                    "Une erreur est survenue lors de la lecture des informations de l'opération de Dynamics 365 : "
                    + "l'identifiant de l'enregistrement Dynamics 365 est vide.");
            }

            var operation = service.Retrieve(
                ame_opration.EntityLogicalName,
                operationId,
                new ColumnSet(new[] {
                    ame_opration.Fields.ame_name,
                    ame_opration.Fields.mcs_filialeId,
                    ame_opration.Fields.ame_Ville,
                    ame_opration.Fields.mcs_operationref
                }));

            return operation;
        }

        /// <summary>
        /// Gets the lead.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="leadId">The lead identifier.</param>
        /// <returns>The lead</returns>
        public static Entity GetLead(IOrganizationService service, Guid leadId)
        {
            if (leadId == Guid.Empty)
            {
                throw new InvalidPluginExecutionException(
                    "Une erreur est survenue lors de la lecture des informations du candidat de Dynamics 365 : "
                    + "l'identifiant de l'enregistrement Dynamics 365 est vide.");
            }

            var lead = service.Retrieve(
                Lead.EntityLogicalName,
                leadId,
                new ColumnSet(new[] {
                    Lead.Fields.FullName,
                    Lead.Fields.EMailAddress1
                }));

            return lead;
        }

        /// <summary>
        /// Gets the space configuration.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="entityLogicalName">Name of the entity logical.</param>
        /// <param name="subsidiaryId">The subsidiary identifier.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentException">
        /// Cannot retrieve space configuration with an empty entity logical name.
        /// or
        /// Cannot retrieve space configuration with an empty subsidiary.
        /// </exception>
        public static string[] GetTags(
            IOrganizationService adminService, string entityLogicalName, EntityReference subsidiaryId)
        {
            if (string.IsNullOrEmpty(entityLogicalName))
            {
                throw new InvalidPluginExecutionException(
                    "Une erreur est survenue lors de la recherche des tags à configurer pour le nouvel espace Plug & Work : "
                    + "le nom logique de l'entité est vide.");
            }

            if (subsidiaryId == null || (subsidiaryId.Id == null && entityLogicalName.Equals(ame_opration.EntityLogicalName)))
            {
                throw new InvalidPluginExecutionException(
                    "Une erreur est survenue lors de la recherche des tags à configurer pour le nouvel espace Plug & Work : "
                    + "la filiale de l'entité est vide.");
            }

            var query = new QueryExpression()
            {
                EntityName = mcs_configurationespacepaw.EntityLogicalName,
                ColumnSet = new ColumnSet(mcs_configurationespacepaw.Fields.mcs_tagsStr),
                Criteria = new FilterExpression()
            };

            var entityLogicalNameEnum =
                (mcs_configurationespacepaw.Enums.mcs_entitylogicalnamecode)Enum.Parse(
                    typeof(mcs_configurationespacepaw.Enums.mcs_entitylogicalnamecode),
                    entityLogicalName,
                    true);

            var conditionLogicalName = new ConditionExpression(
                mcs_configurationespacepaw.Fields.mcs_entitylogicalnamecode,
                ConditionOperator.Equal,
                (int)entityLogicalNameEnum);

            query.Criteria.AddCondition(conditionLogicalName);

            if (entityLogicalName.Equals(ame_opration.EntityLogicalName))
            {
                var conditionSubsidiary = new ConditionExpression(
                    mcs_configurationespacepaw.Fields.mcs_filialeid,
                    ConditionOperator.Equal,
                    subsidiaryId.Id);

                query.Criteria.AddCondition(conditionSubsidiary);
            }

            var entities = adminService.RetrieveMultiple(query).Entities;

            if (entities.Count == 0)
            {
                throw new InvalidPluginExecutionException(
                    "Une erreur est survenue lors de la recherche de la configuration de l'espace : "
                    + "aucune configuration n'a été trouvée pour le nom logique d'entité '" + entityLogicalName + "' et la filiale '" + subsidiaryId.Name + "'.");
            }

            if (entities.Count > 1)
            {
                throw new InvalidPluginExecutionException(
                   "Une erreur est survenue lors de la recherche de la configuration de l'espace : "
                   + "plusieurs occurences de configuration ont été trouvées pour le nom logique d'entité '" + entityLogicalName + "' et la filiale '" + subsidiaryId.Name + "'.");
            }

            var tags = entities[0].GetAttributeValue<string>(mcs_configurationespacepaw.Fields.mcs_tagsStr);
            return tags.Split(',');
        }

        /// <summary>
        /// Gets the associated groups.
        /// </summary>
        /// <param name="adminService">The admin service.</param>
        /// <param name="entityLogicalName">Name of the entity logical.</param>
        /// <param name="subsidiaryId">The subsidiary identifier.</param>
        /// <param name="relationship">The relationship.</param>
        /// <returns></returns>
        /// <exception cref="InvalidPluginExecutionException">
        /// Une erreur est survenue lors de la recherche des groupes utilisateurs à configurer en lecture pour le nouvel espace Plug & Work : "
        ///                     + "le nom logique de l'entité est vide.
        /// or
        /// Une erreur est survenue lors de la recherche des groupes utilisateurs à configurer en lecture pour le nouvel espace Plug & Work : "
        ///                     + "la filiale de l'opération est vide.
        /// </exception>
        public static string[] GetAssociatedGroups(
            IOrganizationService adminService, string entityLogicalName, EntityReference subsidiaryId, string relationship)
        {
            if (string.IsNullOrEmpty(entityLogicalName))
            {
                throw new InvalidPluginExecutionException(
                    "Une erreur est survenue lors de la recherche des groupes utilisateurs à configurer en lecture pour le nouvel espace Plug & Work : "
                    + "le nom logique de l'entité est vide.");
            }

            if (subsidiaryId == null || (subsidiaryId.Id == null && entityLogicalName.Equals(ame_opration.EntityLogicalName)))
            {
                throw new InvalidPluginExecutionException(
                    "Une erreur est survenue lors de la recherche des groupes utilisateurs à configurer en lecture pour le nouvel espace Plug & Work : "
                    + "la filiale de l'opération est vide.");
            }

            if (string.IsNullOrEmpty(relationship))
            {
                throw new InvalidPluginExecutionException(
                    "Une erreur est survenue lors de la recherche des groupes utilisateurs à configurer en lecture pour le nouvel espace Plug & Work : "
                    + "le nom de la relation d'association est vide.");
            }

            var groups = new List<string>();
            var linkFromEntityName = "";
            var linkFromAttributeName = "";
            var linkToAttributeName = "";

            switch (relationship)
            {
                case mcs_configurationespacepaw_groupe_lecteurs.EntityLogicalName:

                    linkFromEntityName = mcs_configurationespacepaw_groupe_lecteurs.EntityLogicalName;
                    linkFromAttributeName = mcs_configurationespacepaw_groupe_lecteurs.Fields.mcs_configurationespacepawid;
                    linkToAttributeName = mcs_configurationespacepaw_groupe_lecteurs.Fields.mcs_plugworksecurityunitid;
                    break;

                case mcs_configurationespacepaw_groupe_contribut.EntityLogicalName:

                    linkFromEntityName = mcs_configurationespacepaw_groupe_contribut.EntityLogicalName;
                    linkFromAttributeName = mcs_configurationespacepaw_groupe_contribut.Fields.mcs_configurationespacepawid;
                    linkToAttributeName = mcs_configurationespacepaw_groupe_contribut.Fields.mcs_plugworksecurityunitid;
                    break;

                case mcs_configurationespacepaw_groupe_lectherit.EntityLogicalName:

                    linkFromEntityName = mcs_configurationespacepaw_groupe_lectherit.EntityLogicalName;
                    linkFromAttributeName = mcs_configurationespacepaw_groupe_lectherit.Fields.mcs_configurationespacepawid;
                    linkToAttributeName = mcs_configurationespacepaw_groupe_lectherit.Fields.mcs_plugworksecurityunitid;
                    break;

                case mcs_configurationespacepaw_groupe_contherit.EntityLogicalName:

                    linkFromEntityName = mcs_configurationespacepaw_groupe_contherit.EntityLogicalName;
                    linkFromAttributeName = mcs_configurationespacepaw_groupe_contherit.Fields.mcs_configurationespacepawid;
                    linkToAttributeName = mcs_configurationespacepaw_groupe_contherit.Fields.mcs_plugworksecurityunitid;
                    break;
            }

            if (string.IsNullOrEmpty(linkFromEntityName))
            {
                throw new InvalidPluginExecutionException(
                    "Une erreur est survenue lors de la recherche des groupes utilisateurs à configurer en lecture pour le nouvel espace Plug & Work : "
                    + "le nom de la relation d'association <" + relationship + "> ne figure pas parmi les noms de relations attendus.");
            }

            var linkGroups = new LinkEntity(
              linkFromEntityName,
              mcs_configurationespacepaw.EntityLogicalName,
              linkFromAttributeName,
              mcs_configurationespacepaw.Fields.mcs_configurationespacepawId,
              JoinOperator.Inner);

            linkGroups.Columns.AddColumns(
                new[] { mcs_configurationespacepaw.Fields.mcs_filialeid,
                    mcs_configurationespacepaw.Fields.mcs_entitylogicalnamecode });

            var entityLogicalNameEnum =
                (mcs_configurationespacepaw.Enums.mcs_entitylogicalnamecode)Enum.Parse(
                    typeof(mcs_configurationespacepaw.Enums.mcs_entitylogicalnamecode),
                    entityLogicalName,
                    true);

            var conditionLogicalName = new ConditionExpression(
                mcs_configurationespacepaw.Fields.mcs_entitylogicalnamecode,
                ConditionOperator.Equal,
                (int)entityLogicalNameEnum);

            linkGroups.LinkCriteria.AddCondition(conditionLogicalName);

            if (entityLogicalName.Equals(ame_opration.EntityLogicalName))
            {
                var conditionSubsidiary = new ConditionExpression(
                mcs_configurationespacepaw.Fields.mcs_filialeid,
                ConditionOperator.Equal,
                subsidiaryId.Id);


                linkGroups.LinkCriteria.AddCondition(conditionSubsidiary);
            }
            
            linkGroups.EntityAlias = "Groups";

            var linkAssoGroups = new LinkEntity(
               mcs_plugworksecurityunit.EntityLogicalName,
               linkFromEntityName,
               mcs_plugworksecurityunit.Fields.mcs_plugworksecurityunitId,
               linkToAttributeName,
               JoinOperator.Inner);

            linkAssoGroups.Columns.AddColumns(linkFromAttributeName);
            linkAssoGroups.EntityAlias = "AssoGroups";
            linkAssoGroups.LinkEntities.Add(linkGroups);

            var query = new QueryExpression()
            {
                EntityName = mcs_plugworksecurityunit.EntityLogicalName,
                ColumnSet = new ColumnSet(mcs_plugworksecurityunit.Fields.mcs_Identifiantpaw),
                Criteria = new FilterExpression()
            };
            query.LinkEntities.Add(linkAssoGroups);

            var entities = adminService.RetrieveMultiple(query).Entities;

            foreach(var entity in entities)
            {
                var groupId = entity.GetAttributeValue<string>(mcs_plugworksecurityunit.Fields.mcs_Identifiantpaw);
                if (!string.IsNullOrWhiteSpace(groupId))
                {
                    if (groupId.StartsWith("g_"))
                    {
                        groups.Add(groupId);
                    }
                    else
                    {
                        groups.Add("g_" + groupId);
                    }
                }
            }
            return groups.ToArray();
        }

        /// <summary>
        /// Gets the template identifier.
        /// </summary>
        /// <param name="adminService">The admin service.</param>
        /// <param name="entityLogicalName">Name of the entity logical.</param>
        /// <param name="subsidiaryId">The subsidiary identifier.</param>
        /// <returns></returns>
        /// <exception cref="InvalidPluginExecutionException">
        /// Une erreur est survenue lors de la recherche du modèle à configurer pour le nouvel espace Plug & Work : "
        ///                     + "le nom logique de l'entité est vide.
        /// or
        /// Une erreur est survenue lors de la recherche du modèle à configurer pour le nouvel espace Plug & Work : "
        ///                     + "la filiale de l'entité est vide.
        /// or
        /// Une erreur est survenue lors de la recherche de la configuration des modèles Plug & Work : "
        ///                     + "aucune configuration n'a été trouvée pour le nom logique d'entité '" + entityLogicalName + "'.
        /// or
        /// Une erreur est survenue lors de la recherche de la configuration des modèles Plug & Work : "
        ///                    + "plusieurs occurences de configuration ont été trouvées pour le nom logique d'entité '" + entityLogicalName + "'.
        /// or
        /// Une erreur est survenue lors de la recherche de la configuration des modèles Plug & Work : "
        ///                    + "l'identifiant du modèle est vide pour le nom logique d'entité '" + entityLogicalName + "'.
        /// </exception>
        public static string GetTemplateId(IOrganizationService adminService, string entityLogicalName, EntityReference subsidiaryId)
        {
            if (string.IsNullOrEmpty(entityLogicalName))
            {
                throw new InvalidPluginExecutionException(
                    "Une erreur est survenue lors de la recherche du modèle à configurer pour le nouvel espace Plug & Work : "
                    + "le nom logique de l'entité est vide.");
            }

            if (subsidiaryId == null || (subsidiaryId.Id == null && entityLogicalName.Equals(ame_opration.EntityLogicalName)))
            {
                throw new InvalidPluginExecutionException(
                    "Une erreur est survenue lors de la recherche du modèle à configurer pour le nouvel espace Plug & Work : "
                    + "la filiale de l'entité est vide.");
            }

            var query = new QueryExpression()
            {
                EntityName = mcs_configurationtemplatepaw.EntityLogicalName,
                ColumnSet = new ColumnSet(mcs_configurationtemplatepaw.Fields.mcs_identifiantpaw),
                Criteria = new FilterExpression()
            };

            var entityLogicalNameEnum =
                (mcs_configurationtemplatepaw.Enums.mcs_entitylogicalnamecode)Enum.Parse(
                    typeof(mcs_configurationtemplatepaw.Enums.mcs_entitylogicalnamecode),
                    entityLogicalName,
                    true);

            var conditionLogicalName = new ConditionExpression(
                mcs_configurationtemplatepaw.Fields.mcs_entitylogicalnamecode,
                ConditionOperator.Equal,
                (int)entityLogicalNameEnum);

            query.Criteria.AddCondition(conditionLogicalName);

            if (entityLogicalName.Equals(ame_opration.EntityLogicalName))
            {
                var conditionSubsidiary = new ConditionExpression(
                    mcs_configurationtemplatepaw.Fields.mcs_filialeid,
                    ConditionOperator.Equal,
                    subsidiaryId.Id);

                query.Criteria.AddCondition(conditionSubsidiary);
            }

            var entities = adminService.RetrieveMultiple(query).Entities;

            if (entities.Count == 0)
            {
                throw new InvalidPluginExecutionException(
                    "Une erreur est survenue lors de la recherche de la configuration des modèles Plug & Work : "
                    + "aucune configuration de modèle n'a été trouvée pour le nom logique d'entité '" + entityLogicalName + "' et la filiale '" + subsidiaryId.Name + "'.");
            }

            if (entities.Count > 1)
            {
                throw new InvalidPluginExecutionException(
                   "Une erreur est survenue lors de la recherche de la configuration des modèles Plug & Work : "
                   + "plusieurs occurences de configuration de modèle ont été trouvées pour le nom logique d'entité '" + entityLogicalName + "' et la filiale '" + subsidiaryId.Name + "'.");
            }

            var templateId = entities[0].GetAttributeValue<string>(mcs_configurationtemplatepaw.Fields.mcs_identifiantpaw);

            if (string.IsNullOrEmpty(templateId))
            {
                throw new InvalidPluginExecutionException(
                   "Une erreur est survenue lors de la recherche de la configuration des modèles Plug & Work : "
                   + "l'identifiant du modèle est vide pour le nom logique d'entité '" + entityLogicalName + "' et la filiale '" + subsidiaryId.Name + "'.");
            }

            return templateId;
        }

        /// <summary>
        /// Determines whether [is entity valid for creation] [the specified admin service].
        /// </summary>
        /// <param name="adminService">The admin service.</param>
        /// <param name="entityLogicalName">Name of the entity logical.</param>
        /// <param name="subsidiaryId">The subsidiary identifier.</param>
        /// <returns>
        ///   <c>true</c> if [is entity valid for creation] [the specified admin service]; otherwise, <c>false</c>.
        /// </returns>
        /// <exception cref="InvalidPluginExecutionException">
        /// Une erreur est survenue lors de la recherche des entités valides pour la création d'un espace Plug & Work : "
        ///                     + "le nom logique de l'entité est vide.
        /// or
        /// Une erreur est survenue lors de la recherche des entités valides pour la création d'un espace Plug & Work : "
        ///                     + "la filiale de l'entité est vide.
        /// </exception>
        public static bool IsEntityValidForCreation(IOrganizationService adminService, string entityLogicalName, EntityReference subsidiaryId)
        {
            if (string.IsNullOrEmpty(entityLogicalName))
            {
                throw new InvalidPluginExecutionException(
                    "Une erreur est survenue lors de la recherche des entités valides pour la création d'un espace Plug & Work : "
                    + "le nom logique de l'entité est vide.");
            }

            if (subsidiaryId == null || (subsidiaryId.Id == null && entityLogicalName.Equals(ame_opration.EntityLogicalName)))
            {
                throw new InvalidPluginExecutionException(
                    "Une erreur est survenue lors de la recherche des entités valides pour la création d'un espace Plug & Work : "
                    + "la filiale de l'entité est vide.");
            }

            var isValid = false;
            var query = new QueryExpression()
            {
                EntityName = mcs_configurationespacepaw.EntityLogicalName,
                ColumnSet = new ColumnSet(false),
                Criteria = new FilterExpression(),
                TopCount = 1
            };

            var entityLogicalNameEnum =
               (mcs_configurationespacepaw.Enums.mcs_entitylogicalnamecode)Enum.Parse(
                   typeof(mcs_configurationespacepaw.Enums.mcs_entitylogicalnamecode),
                   entityLogicalName,
                   true);

            var conditionLogicalName = new ConditionExpression(
                mcs_configurationespacepaw.Fields.mcs_entitylogicalnamecode,
                ConditionOperator.Equal,
                (int)entityLogicalNameEnum);

            query.Criteria.AddCondition(conditionLogicalName);

            if (entityLogicalName.Equals(ame_opration.EntityLogicalName))
            {
                var conditionSubsidiary = new ConditionExpression(
                    mcs_configurationespacepaw.Fields.mcs_filialeid,
                    ConditionOperator.Equal,
                    subsidiaryId.Id);
                query.Criteria.AddCondition(conditionSubsidiary);
            }

            var result = adminService.RetrieveMultiple(query);
            if (result.Entities.Count >= 1)
            {
                isValid = true;
            }
            return isValid;
        }
    }
}
