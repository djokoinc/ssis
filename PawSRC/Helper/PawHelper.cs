﻿using ALITools.ALIT.PaW.Plugins.Model;
using Microsoft.Xrm.Sdk;
using Newtonsoft.Json;
using System.Net;
using System.Text;

namespace ALITools.ALIT.PaW.Plugins.Helper
{
    public static class PawHelper
    {
        /// <summary>
        /// Gets the pw login.
        /// </summary>
        /// <param name="crmConfiguration">The CRM configuration.</param>
        /// <param name="tracingService">The tracing service.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentException">
        /// An error occured while retrieving PW Login: "
        ///                     + "the provided Plug & Work URL is null or empty.
        /// or
        /// An error occured while retrieving PW Login: "
        ///                     + "the provided Plug & Work Username is null or empty.
        /// or
        /// An error occured while retrieving PW Login: "
        ///                     + "the provided Plug & Work Password is null or empty.
        /// </exception>
        /// <exception cref="InvalidPluginExecutionException">An error occured while login into Plug & Work: "
        ///                         + "Web service returned the following message: " + pwResponse.Info</exception>
        public static PawLoginResponse GetPWLogin(CrmConfiguration crmConfiguration, ITracingService tracingService)
        {
            if (string.IsNullOrEmpty(crmConfiguration.PawUrl))
            {
                throw new InvalidPluginExecutionException(
                    "Une erreur est survenue lors de l'authentification au service Plug & Work : "
                    + "l'URL du service Plug & Work est vide.");
            }

            if (string.IsNullOrEmpty(crmConfiguration.PawAdminName))
            {
                throw new InvalidPluginExecutionException(
                    "Une erreur est survenue lors de l'authentification au service Plug & Work : "
                    + "le nom d'utilisateur administrateur Plug & Work est vide.");
            }

            if (string.IsNullOrEmpty(crmConfiguration.PawAdminPassword))
            {
                throw new InvalidPluginExecutionException(
                    "Une erreur est survenue lors de l'authentification au service Plug & Work : "
                    + "le mot de passe administrateur Plug & Work est vide.");
            }

            if (string.IsNullOrEmpty(crmConfiguration.PawDeviceId))
            {
                throw new InvalidPluginExecutionException(
                    "Une erreur est survenue lors de l'authentification au service Plug & Work : "
                    + "l'identifiant machine (uuid) Plug & Work est vide.");
            }

            var pwUrl = string.Format(
                "{0}api/login?username={1}&password={2}&uuid={3}",
                crmConfiguration.PawUrl,
                crmConfiguration.PawAdminName,
                crmConfiguration.PawAdminPassword,
                crmConfiguration.PawDeviceId);
            tracingService.Trace("Login URL: " + pwUrl);

            var pwResponse = new PawLoginResponse();
            string jsonResponse;
            using (WebClient wc = new WebClient())
            {
                wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                jsonResponse = wc.UploadString(pwUrl, "");
            }
            tracingService.Trace("Login returned JSON object: " + jsonResponse);

            if (!string.IsNullOrEmpty(jsonResponse))
            {
                pwResponse = JsonConvert.DeserializeObject<PawLoginResponse>(jsonResponse);

                if (pwResponse.Success.Equals("false")
                    || string.IsNullOrEmpty(pwResponse.Uuid)
                    || string.IsNullOrEmpty(pwResponse.AuthToken))
                {
                    throw new InvalidPluginExecutionException(
                        "Une erreur est survenue lors de l'authentification au service Plug & Work : " + pwResponse.Info);
                }
            }
            return pwResponse;
        }

        /// <summary>
        /// Gets the pw token.
        /// </summary>
        /// <param name="withAdminPrv">if set to <c>true</c> [with admin PRV].</param>
        /// <param name="crmConfiguration">The CRM configuration.</param>
        /// <param name="tracingService">The tracing service.</param>
        /// <returns></returns>
        /// <exception cref="InvalidPluginExecutionException">
        /// Une erreur est survenue lors de l'authentification au service Plug & Work : "
        ///                     + "l'URL du service Plug & Work est vide.
        /// or
        /// Une erreur est survenue lors de l'authentification au service Plug & Work : "
        ///                     + "le nom d'utilisateur administrateur Plug & Work est vide.
        /// or
        /// Une erreur est survenue lors de l'authentification au service Plug & Work : "
        ///                     + "le mot de passe administrateur Plug & Work est vide.
        /// or
        /// Une erreur est survenue lors de l'authentification au service Plug & Work : "
        ///                     + "l'identifiant machine (uuid) Plug & Work est vide.
        /// or
        /// Une erreur est survenue lors de l'authentification au service Plug & Work : "
        ///                     + "l'email de l'utilisateur courant est vide.
        /// or
        /// Une erreur est survenue lors de l'authentification au service Plug & Work : " + pwResponse.Info
        /// </exception>
        public static PawAuthResponse GetPWToken(bool withAdminPrv, CrmConfiguration crmConfiguration, ITracingService tracingService)
        {
            if (string.IsNullOrEmpty(crmConfiguration.PawUrl))
            {
                throw new InvalidPluginExecutionException(
                    "Une erreur est survenue lors de l'authentification au service Plug & Work : "
                    + "l'URL du service Plug & Work est vide.");
            }

            if (string.IsNullOrEmpty(crmConfiguration.PawAdminName))
            {
                throw new InvalidPluginExecutionException(
                    "Une erreur est survenue lors de l'authentification au service Plug & Work : "
                    + "le nom d'utilisateur administrateur Plug & Work est vide.");
            }

            if (string.IsNullOrEmpty(crmConfiguration.PawAdminPassword))
            {
                throw new InvalidPluginExecutionException(
                    "Une erreur est survenue lors de l'authentification au service Plug & Work : "
                    + "le mot de passe administrateur Plug & Work est vide.");
            }

            if (string.IsNullOrEmpty(crmConfiguration.PawDeviceId))
            {
                throw new InvalidPluginExecutionException(
                    "Une erreur est survenue lors de l'authentification au service Plug & Work : "
                    + "l'identifiant machine (uuid) Plug & Work est vide.");
            }

            if (string.IsNullOrEmpty(crmConfiguration.InitiatingUserEmail))
            {
                throw new InvalidPluginExecutionException(
                    "Une erreur est survenue lors de l'authentification au service Plug & Work : "
                    + "l'email de l'utilisateur courant est vide.");
            }

            var parameters =
                "admin_username=" + crmConfiguration.PawAdminName
                + "&admin_password=" + crmConfiguration.PawAdminPassword
                + "&username=" + (withAdminPrv ? crmConfiguration.PawAdminName : crmConfiguration.InitiatingUserEmail.ToLowerInvariant())
                + "&uuid=" + crmConfiguration.PawDeviceId;
            var pwUrl = string.Format("{0}/api/user_token?{1}", crmConfiguration.PawUrl, parameters);

            var pwResponse = new PawAuthResponse();
            string jsonResponse;
            using (WebClient wc = new WebClient())
            {
                wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                jsonResponse = wc.UploadString(pwUrl, "");
            }
            tracingService.Trace("Get User Token returned JSON object: " + jsonResponse);

            if (!string.IsNullOrEmpty(jsonResponse))
            {
                pwResponse = JsonConvert.DeserializeObject<PawAuthResponse>(jsonResponse);

                if (pwResponse.Success.Equals("false")
                    || string.IsNullOrEmpty(pwResponse.UserToken))
                {
                    throw new InvalidPluginExecutionException(
                        "Une erreur est survenue lors de l'authentification au service Plug & Work : " + pwResponse.Info);
                }
            }
            return pwResponse;
        }

        /// <summary>
        /// Creates the space.
        /// </summary>
        /// <param name="url">The URL.</param>
        /// <param name="token">The token.</param>
        /// <param name="uuid">The UUID.</param>
        /// <param name="space">The space.</param>
        /// <param name="tracingService">The tracing service.</param>
        /// <returns></returns>
        /// <exception cref="InvalidPluginExecutionException">
        /// An error occured while creating PW space: the provided Plug & Work URL is null or empty.
        /// or
        /// An error occured while creating PW space: the provided authentication token is null or empty.
        /// or
        /// An error occured while creating PW space: the provided uuid is null or empty.
        /// or
        /// An error occured while creating PW space: the provided space is null.
        /// or
        /// An error occured while creating PW space: the provided space title is null or empty.
        /// or
        /// An error occured while creating PW space: the provided tags array is null or empty.
        /// or
        /// An error occured while creating a new Plug & Work space: "
        ///                            + "Web service returned the following message: " + pwResponse.info
        /// or
        /// An error occured while creating a new Plug & Work space: "
        ///                        + "Web service returned an empty space id
        /// </exception>
        public static string CreateSpace(
            string url,
            string token,
            string uuid,
            SpaceConfigurationManager spaceConfiguration,
            ITracingService tracingService)
        {
            if (string.IsNullOrEmpty(url))
            {
                throw new InvalidPluginExecutionException(
                    "Une erreur est survenue lors de la création de l'espace Plug & Work : "
                    + "l'URL du service Plug & Work est vide.");
            }

            if (string.IsNullOrEmpty(token))
            {
                throw new InvalidPluginExecutionException(
                    "Une erreur est survenue lors de la création de l'espace Plug & Work : "
                    + "le jeton d'authentification au service Plug & Work est vide.");
            }

            if (string.IsNullOrEmpty(uuid))
            {
                throw new InvalidPluginExecutionException(
                    "Une erreur est survenue lors de la création de l'espace Plug & Work : "
                    + "l'UUID d'authentification au service Plug & Work est vide.");
            }

            if (spaceConfiguration == null)
            {
                throw new InvalidPluginExecutionException(
                    "Une erreur est survenue lors de la création de l'espace Plug & Work : "
                    + "la configuration de l'espace Plug & Work est vide.");
            }

            if (string.IsNullOrEmpty(spaceConfiguration.Space.Title))
            {
                throw new InvalidPluginExecutionException(
                    "Une erreur est survenue lors de la création de l'espace Plug & Work : "
                    + "le titre de l'espace Plug & Work est vide.");
            }

            if (spaceConfiguration.Space.Tags == null || spaceConfiguration.Space.Tags.Length == 0)
            {
                throw new InvalidPluginExecutionException(
                    "Une erreur est survenue lors de la création de l'espace Plug & Work : "
                    + "aucun tag n'a été défini pour l'espace Plug & Work.");
            }

            // Null items won't be serialized and sent with the Web request.
            // An empty value causes the request to fail with 503 HTTP error.
            if (spaceConfiguration.Space.Writers.Length == 0) spaceConfiguration.Space.Writers = null;
            if (spaceConfiguration.Space.Readers.Length == 0) spaceConfiguration.Space.Readers = null;
            if (spaceConfiguration.Space.RaciReaders.Length == 0) spaceConfiguration.Space.RaciReaders = null;
            if (spaceConfiguration.Space.RaciWriters.Length == 0) spaceConfiguration.Space.RaciWriters = null;
            spaceConfiguration.Space.Id = null;

            /*
             * Note: 
             * - Labels is a mandatory parameter in request URL as per documentation,
             * however only "tags" declared in body are taken into account when defined.
             * - Right assignments works only if put in request body.
             */
            var pwUrl = string.Format(
                "{0}api/d1/spaces?auth_token={1}&uuid={2}", 
                url,
                token, 
                uuid);

            if (!string.IsNullOrEmpty(spaceConfiguration.TemplateId))
            {
                pwUrl = string.Format("{0}&space_template_id={1}", pwUrl, spaceConfiguration.TemplateId);
            }
            tracingService.Trace("Create space URL: " + pwUrl);

            var pawRequest = new PawSpaceRequest();
            pawRequest.space = spaceConfiguration.Space;

            var jsonRequest = JsonConvert.SerializeObject(
                pawRequest,
                Formatting.None,
                new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
            var bytesRequest = Encoding.UTF8.GetBytes(jsonRequest);
            var encodedRequest = Encoding.UTF8.GetString(bytesRequest);
            tracingService.Trace("Create space Body: " + encodedRequest);

            var jsonResult = "";
            using (WebClient wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                wc.Headers[HttpRequestHeader.ContentType] = "application/json";
                jsonResult = wc.UploadString(pwUrl, encodedRequest);
            }
            tracingService.Trace("Create space returned JSON object: " + jsonResult);

            var spaceId = "";
            if (!string.IsNullOrEmpty(jsonResult))
            {
                var pwResponse = JsonConvert.DeserializeObject<PawSpaceResponse>(jsonResult);
                if (string.IsNullOrEmpty(pwResponse.Id))
                {
                    if (!string.IsNullOrEmpty(pwResponse.Info))
                    {
                        throw new InvalidPluginExecutionException(
                           "Une erreur est survenue lors de la création de l'espace Plug & Work : " + pwResponse.Info);
                    }
                    throw new InvalidPluginExecutionException("Une erreur est survenue lors de la création de l'espace Plug & Work.");
                }
                spaceId = pwResponse.Id;
            }
            return spaceId;
        }

        /// <summary>
        /// Gets the space.
        /// </summary>
        /// <param name="url">The URL.</param>
        /// <param name="token">The token.</param>
        /// <param name="uuid">The UUID.</param>
        /// <param name="criteria">The criteria.</param>
        /// <param name="tracingService">The tracing service.</param>
        /// <returns></returns>
        /// <exception cref="InvalidPluginExecutionException">
        /// Une erreur est survenue lors de la recherche de l'espace Plug & Work : "
        ///                     + "l'URL du service Plug & Work est vide.
        /// or
        /// Une erreur est survenue lors de la recherche de l'espace Plug & Work : "
        ///                     + "le jeton d'authentification au service Plug & Work est vide.
        /// or
        /// Une erreur est survenue lors de la recherche de l'espace Plug & Work : "
        ///                     + "l'UUID d'authentification au service Plug & Work est vide.
        /// or
        /// Une erreur est survenue lors de la recherche de l'espace Plug & Work : "
        ///                     + "les critères de recherche sont vides.
        /// or
        /// Une erreur est survenue lors de la recherche de l'espace Plug & Work : "
        ///                         + "aucun espace n'a été trouvé.
        /// or
        /// Une erreur est survenue lors de la recherche de l'espace Plug & Work : "
        ///                         + "plusieurs espaces ont été trouvés.
        /// </exception>
        public static PawSpace GetSpace(
            string url,
            string token,
            string uuid,
            string criteria,
            ITracingService tracingService)
        {
            if (string.IsNullOrEmpty(url))
            {
                throw new InvalidPluginExecutionException(
                    "Une erreur est survenue lors de la recherche de l'espace Plug & Work : "
                    + "l'URL du service Plug & Work est vide.");
            }

            if (string.IsNullOrEmpty(token))
            {
                throw new InvalidPluginExecutionException(
                    "Une erreur est survenue lors de la recherche de l'espace Plug & Work : "
                    + "le jeton d'authentification au service Plug & Work est vide.");
            }

            if (string.IsNullOrEmpty(uuid))
            {
                throw new InvalidPluginExecutionException(
                    "Une erreur est survenue lors de la recherche de l'espace Plug & Work : "
                    + "l'UUID d'authentification au service Plug & Work est vide.");
            }

            if (string.IsNullOrEmpty(criteria))
            {
                throw new InvalidPluginExecutionException(
                    "Une erreur est survenue lors de la recherche de l'espace Plug & Work : "
                    + "les critères de recherche sont vides.");
            }

            var pwUrl = string.Format(
                "{0}api/d1/spaces?auth_token={1}&uuid={2}&exact=true&{3}",
                url,
                token,
                uuid,
                criteria);
            tracingService.Trace("Get Space URL criteria: " + criteria);
            tracingService.Trace("Get Space URL: " + pwUrl);

            var pwResponse = new PawGetSpaceResponse();
            var jsonResponse = "";
            using (WebClient wc = new WebClient())
            {
                wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                jsonResponse = wc.DownloadString(pwUrl);
            }
            tracingService.Trace("Get Space returned JSON object: " + jsonResponse);

            if (!string.IsNullOrEmpty(jsonResponse))
            {
                pwResponse = JsonConvert.DeserializeObject<PawGetSpaceResponse>(jsonResponse);

                if (pwResponse.Count == 0)
                {
                    throw new InvalidPluginExecutionException(
                        "Une erreur est survenue lors de la recherche de l'espace Plug & Work : "
                        + "aucun espace n'a été trouvé.");
                }

                if (pwResponse.Count > 1)
                {
                    throw new InvalidPluginExecutionException(
                        "Une erreur est survenue lors de la recherche de l'espace Plug & Work : "
                        + "plusieurs espaces ont été trouvés.");
                }
            }
            return pwResponse.PawSpaces[0];
        }
    }
}
