﻿using Newtonsoft.Json;

namespace ALITools.ALIT.PaW.Plugins.Model
{
    public class PawSpace
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("tags")]
        public string[] Tags { get; set; }

        [JsonProperty("writers")]
        public string[] Writers { get; set; }

        [JsonProperty("readers")]
        public string[] Readers { get; set; }

        [JsonProperty("raci_readers")]
        public string[] RaciReaders { get; set; }

        [JsonProperty("raci_writers")]
        public string[] RaciWriters { get; set; }

        [JsonProperty("superfields")]
        public PawSuperfield Superfields { get; set; }
    }
}
