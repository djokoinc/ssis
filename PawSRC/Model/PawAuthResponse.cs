﻿using Newtonsoft.Json;

namespace ALITools.ALIT.PaW.Plugins.Model
{
    public class PawAuthResponse
    {
        [JsonProperty("success")]
        public string Success { get; set; }

        [JsonProperty("info")]
        public string Info { get; set; }

        [JsonProperty("login")]
        public string Login { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("user_token")]
        public string UserToken { get; set; }

        [JsonProperty("device_uuid")]
        public string DeviceUuid { get; set; }

        [JsonProperty("device_auth_token")]
        public string DeviceAuthToken { get; set; }
    }
}
