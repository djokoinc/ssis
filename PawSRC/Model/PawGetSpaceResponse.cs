﻿using Newtonsoft.Json;

namespace ALITools.ALIT.PaW.Plugins.Model
{
    public class PawGetSpaceResponse
    {
        [JsonProperty("count")]
        public int Count { get; set; }

        [JsonProperty("limit")]
        public int Limit { get; set; }

        [JsonProperty("results")]
        public PawSpace[] PawSpaces { get; set; }
    }
}
