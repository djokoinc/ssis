﻿using Newtonsoft.Json;

namespace ALITools.ALIT.PaW.Plugins.Model
{
    public class PawSpaceRequest
    {
        [JsonProperty("space")]
        public PawSpace space { get; set; }
    }
}
