﻿using Newtonsoft.Json;
using System;

namespace ALITools.ALIT.PaW.Plugins.Model
{
    public class PawSpaceResponse
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("created_at")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("success")]
        public string Success { get; set; }

        [JsonProperty("info")]
        public string Info { get; set; }
    }
}
