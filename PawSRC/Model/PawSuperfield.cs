﻿using Newtonsoft.Json;

namespace ALITools.ALIT.PaW.Plugins.Model
{
    public class PawSuperfield
    {
        [JsonProperty("_entityid")]
        public string EntityId { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }
    }
}