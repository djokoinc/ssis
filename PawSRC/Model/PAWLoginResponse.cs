﻿using Newtonsoft.Json;

namespace ALITools.ALIT.PaW.Plugins.Model
{
    public class PawLoginResponse
    {
        [JsonProperty("success")]
        public string Success { get; set; }

        [JsonProperty("info")]
        public string Info { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("login")]
        public string Login { get; set; }

        [JsonProperty("channel_key")]
        public string ChannelKey { get; set; }

        [JsonProperty("uuid")]
        public string Uuid { get; set; }

        [JsonProperty("auth_token")]
        public string AuthToken { get; set; }
    }
}
