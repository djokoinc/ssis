﻿using Microsoft.Xrm.Sdk;
using System;

namespace ALITools.ALIT.PaW.Plugins.Model
{
    public class CrmConfiguration
    {
        public string PawUrl { get; private set; }
        public string PawAdminName { get; private set; }
        public string PawAdminPassword { get; private set; }
        public string PawDeviceId { get; private set; }
        public string InitiatingUserEmail { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrmConfiguration"/> class.
        /// </summary>
        /// <param name="pawUrl">The paw URL.</param>
        /// <param name="pawAdminName">Name of the paw admin.</param>
        /// <param name="pawAdminPassword">The paw admin password.</param>
        /// <param name="pawDeviceId">The paw device id.</param>
        /// <param name="initiatingUserEmail">The initiating user email.</param>
        public CrmConfiguration(string pawUrl, string pawAdminName, string pawAdminPassword, string pawDeviceId, string initiatingUserEmail)
        {
            // Validate PAW URL
            if (string.IsNullOrEmpty(pawUrl))
            {
                throw new InvalidPluginExecutionException(
                    "Une erreur est survenue lors de l'initialisation de la configuration Dynamics 365 : "
                    + "l'URL du service Plug & Work Plug & Work est vide.");
            }

            // Validate PAW administrator username
            if (string.IsNullOrEmpty(pawAdminName))
            {
                throw new InvalidPluginExecutionException(
                    "Une erreur est survenue lors de l'initialisation de la configuration Dynamics 365 : "
                    + "le nom d'utilisateur de l'administrateur Plug & Work est vide.");
            }

            // Validate PAW administrator password
            if (string.IsNullOrEmpty(pawAdminPassword))
            {
                throw new InvalidPluginExecutionException(
                    "Une erreur est survenue lors de l'initialisation de la configuration Dynamics 365 : "
                    + "le mot de passe de l'administrateur Plug & Work est vide.");
            }

            // Validate PAW administrator password
            if (string.IsNullOrEmpty(pawDeviceId))
            {
                throw new InvalidPluginExecutionException(
                    "Une erreur est survenue lors de l'initialisation de la configuration Dynamics 365 : "
                    + "l'identifiant machine (uuid) Plug & Work est vide.");
            }

            // Validate initiating user email
            if (string.IsNullOrEmpty(initiatingUserEmail))
            {
                throw new InvalidPluginExecutionException(
                    "Une erreur est survenue lors de l'initialisation de la configuration Dynamics 365 : "
                    + "l'adresse email de l'utilisateur courant est vide.");
            }

            PawUrl = pawUrl;
            PawAdminName = pawAdminName;
            PawAdminPassword = pawAdminPassword;
            PawDeviceId = pawDeviceId;
            InitiatingUserEmail = initiatingUserEmail;
        }
    }
}
