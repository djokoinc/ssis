﻿ 
param
(
    [Parameter(Mandatory=$true)][string]$ServerName,
    [Parameter(Mandatory=$true)][string]$JobName,
    [Parameter(Mandatory=$false)][string]$StepName,
    [int]$Delai
)

### Enum 

Add-Type -TypeDefinition @"
   public enum ExecuteJobSqlError
   {
      ServerNF,
      JobServerNF,
      JobSqlServerNF,
      JobStepNF
   }
"@

### Function

function Get-ErrorMessage
{
    param 
    ( 
        [Parameter(Mandatory=$true)]
        $errorName,
        [Parameter(Mandatory=$false)]
        $errorArgument
    )

    $errorMessage = "[[ No error found with name '$($errorName)' ]]"
    if(!$errorArgument){
        $errorArgument = "[[ No argument pass ]]"
    }    
     
    switch($errorName)
    {
        "ServerNF" {
	        $errorMessage = "Server error - can't access to server with name $($errorArgument)"
	        continue
        }
        "JobServerNF" {
	        $errorMessage = "Server error - no Job Server found on server : '$($errorArgument)'"
	        continue
        }
        "JobSqlServerNF" {
	        $errorMessage = "Job Sql error - can't find job sql agent with name : 
                '$($errorArgument)'"
            continue
        } 
        "JobStepNF" {
	        $errorMessage = "Job Sql error - can't find step with name : 
                '$($errorArgument)'"
            continue
        }
    }
    return $errorMessage
}

function ExitWhitError
{
    param
    (
        [Parameter(Mandatory=$true)]
        $errorName,
        [Parameter(Mandatory=$false)]
        $errorArgument
    )

    [int]$iError=[ExecuteJobSqlError]::$errorName
    [string]$mssError= Get-ErrorMessage -errorName $errorName -errorArgument $errorArgument
    
    write-verbose  "------>> Error >> Name : $($errorName) - Code : $($iError)"
    Exit 1

}

function ExitWithCode 
{ 
    param 
    ( 
        $exitcode 
    )

    $host.SetShouldExit($exitcode) 
    exit 
}

function Get-SQLJobStatus
{
    param ([Microsoft.SqlServer.Management.SMO.Server]$server, [string]$JobName)

    
    # used to allow piping of more than one job name to function
    if($JobName)
    {
        foreach($j in $server.JobServer.Jobs)
        {
            $server.JobServer.Jobs | where {$_.Name -match $JobName} | Select Name, CurrentRunStatus
        }
    }
    else #display all jobs for the instance
    {
        $server.JobServer.Jobs | Select Name, CurrentRunStatus
    } #end of Get-SQLJobStatus
}

function Execute-SQLJob-Until
{

    param
    (
        [Microsoft.SqlServer.Management.Smo.Agent.Job]
        [Parameter(Mandatory=$true)]
        $job,
        [string]
        [Parameter(Mandatory=$false)]
        $stepName,
        [int]
        [Parameter(Mandatory=$false)]
        $delai,
        [string]
        [Parameter(Mandatory=$false)]
        $jobName

    )

    [string]$SuccessJob=[Microsoft.SqlServer.Management.Smo.Agent.CompletionResult]::Succeeded
    
    if(!$job)
    {
        ExitWhitError -errorName $([ExecuteJobSqlError]::JobSqlServerNF) -errorArgument $jobName
    }

    
    if(![string]::IsNullOrWhiteSpace($stepName))
    {
        $step = $job.JobSteps["$stepName"]
        ExitWhitError -errorName $([ExecuteJobSqlError]::JobStepNF) -errorArgument $stepName
    }
	
    if($step -ne '') 
    {
	    $job.Start($step)
    }
    else 
    {
	    $job.Start()
    }

    Write-Verbose "Job $($jobName) started"
    $i = 0
    [int]$result=1
    
    do 
    {
        write-verbose "REFRESHING..."
        $job.Refresh();
        $i++
        Start-Sleep -Seconds $delai
        $jobrunning=$job.CurrentRunStatus.ToString();       
        $date=Get-Date
        write-verbose "Job $($jobName) Processing--Run Step:$($job.CurrentRunStep) Status:$($job.CurrentRunStatus.ToString())... at $($date)"
    }
    while ($job.CurrentRunStatus.ToString() -ne "Idle")

    if ($job.LastRunOutcome -eq $SuccessJob)
    {
        write-verbose "Job Processing Done"
        $result=0
    }
    else
    {
        write-verbose "Job Processing ensure an error - Finsih with status : $($job.LastRunOutcome)"
    }

    return $result
}

#### Config

if(!$Delai){
    $Delai=5
}

### Context

write-verbose "Context >>> Starting SQL Agent Job '$($JobName)' on Server '$($ServerName)'"
write-verbose "Context >>> JobName : '$($JobName)'"
$date=Get-Date
write-verbose "Context >>> It is now : '$($date)' - refreshing time limit : $($Delai)"

### Process

# Check context

[System.Reflection.Assembly]::LoadWithPartialName('Microsoft.SqlServer.SMO') | out-null
$srv = New-Object Microsoft.SqlServer.Management.SMO.Server("$ServerName")
if (!$srv)
{
    ExitWhitError -errorName $([ExecuteJobSqlError]::ServerNF) -errorArgument $ServerName
}
if (!$srv.JobServer -or !$srv.Jobserver.jobs)
{
    ExitWhitError -errorName $([ExecuteJobSqlError]::JobServerNF) -errorArgument $ServerName
}
$job = $srv.jobserver.jobs["$JobName"]
if(!$job)
{
    ExitWhitError -errorName $([ExecuteJobSqlError]::JobSqlServerNF) -errorArgument $JobName
}

# Execute Job

return Execute-SQLJob-Until -job $job -jobName $JobName -stepName $StepName -delai $Delai


