﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SendToSeLoger
{
    public class SeLogerContext
    {
        #region property 
        public const string ERROR_START = "============================ ERRORS ============================";

        public List<string> Errors { get; set; }

        public bool uniqueProcess { get; set; } = false;

        public bool verbose { get; set; } = false;

        public string ftpProtocoleSuffix { get; set; } = "ftp://";

        #region Property From Config
        public string dataFolderPath;
        public string archiveFolderPath;
        public string tmpFolderName;
        public string tmpFolderPath;
        public string versionSpecification;
        public string versionAppInli;
        public string nameAppInli;
        private string ftpHost;
        private string ftpPort;
        private string ftpUser;
        private string ftpPassword;        
        #endregion

        #endregion

        #region Constructeur

        public SeLogerContext()
        {
            Errors = new List<string>();
        }

        #endregion

        // Function to call to Init 
        public void Init(string[] args)
        {
            // Get config parameter from App file
            InitConfig();

            // Handle arguments which can change config value
            HandleArgs(args);

            // Init Tmp archive folder
            InitTmpArchiveFolder();

            // Throw Error and exit the program if there is one 
            ThrowErrors();
        }

        #region Handle Program Arguments 

        internal void HandleArgs(string[] args)
        {
            if (args.Length > 0)
            {
                if (args[0] == "--help" || args[0] == "-h" || args[0] == "help" || args[0] == "/?")
                {
                    Helps();
                }

                if (!(args[0].StartsWith("-") || args[0].StartsWith("--")))
                {
                    throw new ArgumentException("Program Argument must begin by - or --");
                }                

                for (int i = 0; i < args.Length; i++)
                {
                    string arg = args[i];

                    if (!(arg.StartsWith("-") || arg.StartsWith("--")))
                    {
                        continue;
                    }

                    HandleArgValue(arg, args, i);
                }
            }
        }

        private void HandleArgValue(string arg, string[] args, int indexArg)
        {
            if(arg == "-u" || arg == "--unique")
            {
                uniqueProcess = true;
            } else if(arg == "-s" || arg == "--source")
            {
                TryGetArgsValue(indexArg, args, ref dataFolderPath);                
            } else if (arg == "-a" || arg == "--archive")
            {
                TryGetArgsValue(indexArg, args, ref archiveFolderPath);
            } else if (arg == "-v" || arg == "--verbose")
            {
                verbose = true;
            }
        }

        private void TryGetArgsValue(int index, string[] args, ref string param)
        {
            
            if (index + 1 > args.Length)
            {
                Errors.Add(string.Format("Argument {0} need a value", args[index]));
            }
            else
            {
                if(string.IsNullOrWhiteSpace(args[index + 1]))
                {
                    Errors.Add(string.Format("Argument {0} can't be empty or white space", args[index]));
                    return;
                }
                param = args[index + 1];
            }
        }

        #endregion

        public void InitConfig()
        {
            if (!TryGetSetting("DataFolderPath", out dataFolderPath))
            {
                Errors.Add("Config Error => DataFolderPath");
            }

            if (!TryGetSetting("ArchiveFolderPath", out archiveFolderPath))
            {
                Errors.Add("Config Error => archiveFolderPath");
            }

            if (!TryGetSetting("TMPArchiveFolderName", out tmpFolderName))
            {
                Errors.Add("Config Error => TMPArchiveFolderName");
            }

            if (!TryGetSetting("VersionSpecificationSeLoger", out versionSpecification))
            {
                Errors.Add("Config Error => VersionSpecificationSeLoger");
            }

            if (!TryGetSetting("VersionAppInli", out versionAppInli))
            {
                Errors.Add("Config Error => VersionAppInli");
            }

            if (!TryGetSetting("NameAppInli", out nameAppInli))
            {
                Errors.Add("Config Error => NameAppInli");
            }

            if (!TryGetSetting("SeLogerSFTPHost", out ftpHost))
            {
                Errors.Add("Config Error => SeLogerSFTPHost");
            }

            if (!TryGetSetting("SeLogerSFTPUser", out ftpUser))
            {
                Errors.Add("Config Error => SeLogerSFTPUser");
            }

            if (!TryGetSetting("SeLogerSFTPPwd", out ftpPassword))
            {
                Errors.Add("Config Error => SeLogerSFTPPwd");
            }

            if (!TryGetSetting("SeLogerSFTPPort", out ftpPort))
            {
                Errors.Add("Config Error => SeLogerSFTPPort");
            }
        }

        private static bool TryGetSetting(string key, out string value)
        {
            value = ConfigurationManager.AppSettings[key];

            if ((string.IsNullOrEmpty(value) || string.IsNullOrWhiteSpace(value)))
            {
                Console.WriteLine("Setting Error => Name : {0}",
                    value);
            }

            return !(string.IsNullOrEmpty(value) || string.IsNullOrWhiteSpace(value));
        }

        public void InitTmpArchiveFolder()
        {
            DirectoryInfo TMPDir;
            string exePath = AppDomain.CurrentDomain.BaseDirectory;
            DirectoryInfo dirExe = new DirectoryInfo(exePath);
            DirectoryInfo[] dirsTMP = dirExe.GetDirectories(tmpFolderName);

            if (dirsTMP.Count() < 1)
            {
                TMPDir = dirExe.CreateSubdirectory(tmpFolderName);
            }
            else
            {
                TMPDir = dirsTMP[0];
            }

            tmpFolderPath = TMPDir.FullName;
        }

        public FtpWebRequest GetFtpUpLoadRequest(string fileName)
        {            
            if (string.IsNullOrWhiteSpace(ftpHost)
                || string.IsNullOrWhiteSpace(ftpPort)
                || string.IsNullOrWhiteSpace(ftpUser)
                || string.IsNullOrWhiteSpace(ftpPassword))
            {
                throw new Exception("SendToSeLoger : SFTP parameters can't be empty");
            }
            string uriFtp = ftpProtocoleSuffix;
            uriFtp += ftpHost.EndsWith(@"/") ? ftpHost + fileName : ftpHost + @"/" + fileName;
            
            FtpWebRequest ftpClient = (FtpWebRequest)WebRequest.Create(uriFtp);
            ftpClient.Method = WebRequestMethods.Ftp.UploadFile;
            // This example assumes the FTP site uses anonymous logon.  
            ftpClient.Credentials = new NetworkCredential(ftpUser, ftpPassword);
            
            return ftpClient;
        }
        
        public override string ToString()
        {
            string formatTxt = "SeLogerContext :"
                + "{0}dataFolderPath : {1}"
                + "{0}archiveFolderPath : {2}"
                + "{0}tmpDirectoryPath : {3}"
                + "{0}Errors :{0}{4}";


            return string.Format(formatTxt,
                Environment.NewLine,
                dataFolderPath,
                archiveFolderPath,
                tmpFolderPath,
                toErrorString());
        }

        public string toErrorString()
        {
            return string.Join(Environment.NewLine, Errors);
        }

        public void ThrowErrors(bool mustExit = true)
        {
            if (Errors.Count > 0)
            {
                Console.WriteLine("{2}{1}context.Errors : {1}{0}",
                    this.toErrorString(),
                    Environment.NewLine,
                    ERROR_START);

                if (mustExit)
                {
                    Environment.Exit(1);
                } else
                {
                    Environment.ExitCode = 1;
                }                
            }
        }

        public void Helps()
        {
            string helps = "===================> SendToSeLoger.exe {0}{0}"
                + "Options :{0}"
                + "    --verbose  || -v {0}"
                + "    --unique   || -u {0}"
                + "        Use to tell that the directory Seloger given by argument or"
                + " config is the directory to send. Use this option to send a unique directory,"
                + "for example to send again a agency directory{0}"
                + "    --source   || -s <Path to Seloger directory> {0}"
                + "        Use option (-u || --unique) if the directory given is an agency directory (with mandatory file to send) {0}"
                + "    --archvive || -a <Path to Archive directory> {0}"
                + "        Use to precise a new archive folder.{0}";

            Console.WriteLine(helps, Environment.NewLine);
            Environment.Exit(0);
        }
    }
}
