﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace SendToSeLoger
{
    public sealed class Log
    {
        private static readonly Log instance = new Log();

        public static Log Instance
        {
            get
            {
                return instance;
            }
        }

        public List<string> logContents { get; set; }

        public string logFilePath { get; set; }

        public string logFileName { get; set; }

        public string dateFormat { get; set; }

        public Out outMethod { get; set; } = Out.Console;

        public enum Out
        {
            File,
            Console
        }

        static Log()
        {
        }

        private Log()
        {
            dateFormat = "dd-MM-yyyy HH:mm:ss.fff";
            logFileName = Assembly.GetExecutingAssembly().GetName().Name + ".log";
            logContents = new List<string>();

            DirectoryInfo dirExe = new DirectoryInfo(Environment.CurrentDirectory);

            logFilePath = dirExe.FullName.EndsWith(@"\") ? 
                dirExe.FullName + logFileName 
                : dirExe.FullName + @"\" + logFileName;

        }

        public static void setOutMethod(Out outMethodParam, string filePath = null)
        {
            instance.outMethod = outMethodParam;

            if(outMethodParam == Out.File)
            {
                instance.InitFile(filePath);
            }
        }

        private void InitFile(string filePath)
        {
            if (string.IsNullOrWhiteSpace(filePath))
            {
                if (string.IsNullOrWhiteSpace(logFilePath))
                {
                    throw new Exception("Log : filePath argument is null and we couldn't create log file with defaut path");
                }
                filePath = logFilePath;
            } 
            
            try
            {
                if (!File.Exists(filePath))
                {
                    File.Create(filePath);                    
                }                
                logFilePath = filePath;
            }
            catch(Exception e)
            {
                throw new Exception(
                    string.Format("Log file '{0}' couldn't be created - Exception.Message = '{1}'",
                    filePath,
                    e.Message));
            }
        }

        private void Add(string logLine)
        {
            logContents.Add(logLine);

            if(instance.outMethod == Out.Console)
            {
                Console.WriteLine(logLine);
            }

            if (instance.outMethod == Out.File && File.Exists(logFilePath))
            {
                using (StreamWriter writer = File.AppendText(logFilePath))
                {
                    writer.WriteLine(logLine);
                }

            }
        }

        public static void Trace(string logTxt)
        {
            instance.Add(logTxt);
        }
    }
}
