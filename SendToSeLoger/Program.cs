﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;

namespace SendToSeLoger
{
    class Program
    {
        
        static void Main(string[] args)
        {
            SeLogerContext context = new SeLogerContext();
            List<string> DirectoryNotSend = new List<string>();
            List<string> DirectorySend = new List<string>();

            context.Init(args);
            
            if (context.uniqueProcess)
            {
                UniqueProcess(context, ref DirectoryNotSend, ref DirectorySend);
            } else
            {
                MultipleProcess(context, ref DirectoryNotSend, ref DirectorySend);
            }

            ShowResult(context, DirectoryNotSend, DirectorySend);
            context.ThrowErrors(false);

            if (context.verbose)
            {
                Console.WriteLine("Press a key to exit!");
                Console.ReadKey();
            }            
        }

        internal static void MultipleProcess(SeLogerContext context, ref List<string> DirectoryNotSend, ref List<string> DirectorySend)
        {
            DirectoryInfo dirSeloger = new DirectoryInfo(context.dataFolderPath);
            if (!dirSeloger.Exists)
            {
                Console.WriteLine("Argument passed do not represent a existing folder, please pass an existing folder!");
            }

            DirectoryInfo[] agencyDirs = dirSeloger.GetDirectories();
            if (agencyDirs.Length <= 0)
            {
                Console.WriteLine("Directory passed do not contain any agency directory.");
            }

            foreach (DirectoryInfo agencyDir in agencyDirs)
            {
                bool isFolderSend = false;
                if (SeLogerFolder.IsSeLogerDir(agencyDir, SeLogerFolder.mandatoryFileName, true))
                {
                    SeLogerProccess agencyProcccess = new SeLogerProccess(agencyDir.FullName);
                    try
                    {
                        isFolderSend = agencyProcccess.Proccessing(context);
                        if (!isFolderSend)
                        {
                            context.Errors.Add(SeLogerProccess.ErrorMesssage.sendError(agencyDir.FullName));
                        }

                    } catch( Exception e)
                    {
                        // Handle exception
                        context.Errors.Add(SeLogerProccess.ErrorMesssage.sendError(agencyDir.FullName, e.Message));
                    }
                }

                if (!isFolderSend)
                {
                    DirectoryNotSend.Add(string.Format("Directory : {0}.",
                        agencyDir.FullName));
                } else
                {
                    DirectorySend.Add(string.Format("Directory : {0}.",
                        agencyDir.FullName));
                }
            }
        }

        internal static void UniqueProcess(SeLogerContext context, ref List<string> DirectoryNotSend, ref List<string> DirectorySend)
        {
            DirectoryInfo agencyDir = new DirectoryInfo(context.dataFolderPath);
            if (!agencyDir.Exists)
            {
                Console.WriteLine("Argument passed do not represent a existing folder, please pass an existing folder!");
            }

            bool isFolderSend = false;

            if (SeLogerFolder.IsSeLogerDir(agencyDir, SeLogerFolder.mandatoryFileName, true))
            {                
                SeLogerProccess agencyProcccess = new SeLogerProccess(agencyDir.FullName);
                try {
                    isFolderSend = agencyProcccess.Proccessing(context);
                    if (!isFolderSend)
                    { 
                        context.Errors.Add(SeLogerProccess.ErrorMesssage.sendError(agencyDir.FullName));
                    }
                }
                catch (Exception e)
                {
                    // Handle exception
                    context.Errors.Add(SeLogerProccess.ErrorMesssage.sendError(agencyDir.FullName, e.Message));
                }
            }

            if (!isFolderSend)
            {
                DirectoryNotSend.Add(string.Format("Directory : {0}.",
                    agencyDir.FullName));
            } else
            {
                DirectorySend.Add(string.Format("Directory : {0}.",
                    agencyDir.FullName));
            }
        }        

        internal static void ShowResult(SeLogerContext context, List<string> DirectoryNotSend, List<string> DirectorySend)
        {
            string resume = string.Format("========================================================={0}"
                + "===========> Proccess SendToSeLoger - result <==========={0}"
                + "===> Folder send {0}{1}{0}"
                + "===> Folder not send {0}{2}{0}",
                Environment.NewLine,
                string.Join(Environment.NewLine, DirectorySend),
                string.Join(Environment.NewLine, DirectoryNotSend)
                );

            Log.Trace(resume);

            if (!context.verbose)
            {
                Console.WriteLine("==================== Proccess Resume ===================="
                    + "{0}{1}{0}"
                    + "==================== Proccess Resume end ====================",
                    Environment.NewLine,
                    resume);
            }
        }
    }
}
