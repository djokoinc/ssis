﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SendToSeLoger
{
    public class SeLogerFolder
    {
        
        public DirectoryInfo sourceFolder { get; set; }
        public static string extensionCompressFile = ".zip";
        public static string mandatoryFileName = "Annonces.csv";
        public static string configFileName = "Config.txt";
        public static string photosConfigFileName = "Photos.cfg";

        public SeLogerFolder(string folderName)
        {
            sourceFolder = new DirectoryInfo(folderName);

            if (!sourceFolder.Exists)
                throw new Exception("SeLogerFolder => folder not found, path : " + folderName);
        }

        public DirectoryInfo CopyTo(string destinationPath)
        {
            if(string.IsNullOrWhiteSpace( destinationPath ) 
                || !Directory.Exists( destinationPath ) )
            {
                return null;
            }

            if (!sourceFolder.Exists)
            {
                return null;
            }

            destinationPath = (destinationPath.EndsWith(@"\") ? destinationPath : destinationPath + @"\") + sourceFolder.Name;
            
            if (CopyFolderContents(this.sourceFolder.FullName, destinationPath) && Directory.Exists(destinationPath))
            {
                return new DirectoryInfo(destinationPath);
            }
            else
            {
                return null;
            }
        }

        public static bool CopyFolderContents(string sourcePath, string destinationPath)
        {
            sourcePath = sourcePath.EndsWith(@"\") ? sourcePath : sourcePath + @"\";
            destinationPath = destinationPath.EndsWith(@"\") ? destinationPath : destinationPath + @"\";

            try
            {
                if (Directory.Exists(sourcePath))
                {
                    if (Directory.Exists(destinationPath) == false)
                    {
                        Directory.CreateDirectory(destinationPath);
                    }

                    foreach (string files in Directory.GetFiles(sourcePath))
                    {
                        FileInfo fileInfo = new FileInfo(files);
                        fileInfo.CopyTo(string.Format(@"{0}\{1}", destinationPath, fileInfo.Name), true);
                    }

                    foreach (string drs in Directory.GetDirectories(sourcePath))
                    {
                        DirectoryInfo directoryInfo = new DirectoryInfo(drs);
                        if (CopyFolderContents(drs, destinationPath + directoryInfo.Name) == false)
                        {
                            return false;
                        }
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool AddFiles(SeLogerContext context)
        {
            return (addConfigFile(context) && addPhotosConfigFile(context)) ;
        }

        public bool addConfigFile(SeLogerContext context)
        {
            try
            {
                string configPath = sourceFolder.FullName.EndsWith(@"\") ? sourceFolder.FullName + configFileName : sourceFolder.FullName + @"\" + configFileName;
                // Create a file to write to.
                using (StreamWriter sw = File.CreateText(configPath))
                {
                    sw.WriteLine(
                        string.Format("Version={0}",
                        context.versionSpecification));

                    sw.WriteLine(
                        string.Format("Application={0}_{1}",
                        context.nameAppInli,
                        context.versionAppInli));

                    sw.WriteLine("Devise=Euro");
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool addPhotosConfigFile(SeLogerContext context)
        {
            try
            {
                string photosConfigPath = sourceFolder.FullName.EndsWith(@"\") ? sourceFolder.FullName + photosConfigFileName : sourceFolder.FullName + @"\" + photosConfigFileName;
                // Create a file to write to.
                using (FileStream fs = File.Create(photosConfigPath))
                {
                    Byte[] text = new UTF8Encoding(true).GetBytes("Mode=URL");
                    // Add some information to the file.
                    fs.Write(text, 0, text.Length);
                }

                return true;
            }
            catch 
            {
                return false;
            }
        }

        public string getCompressFolder(SeLogerContext context)
        {
            if (!Directory.Exists(sourceFolder.FullName))
            {
                context.Errors.Add(SeLogerProccess.ErrorMesssage.createDirError(sourceFolder.FullName));
            }

            string zipPath = sourceFolder.FullName + extensionCompressFile;

            ZipFile.CreateFromDirectory(sourceFolder.FullName, zipPath);

            return File.Exists(zipPath) ? zipPath : null ;
        }

        public bool IsValidDir(bool withMultipleLine = false)
        {
            if (string.IsNullOrEmpty(mandatoryFileName))
            {
                throw new Exception("IsSeLogerDir -> MandatoryFileName can't be null or empty");
            }

            if (!sourceFolder.Exists)
            {
                throw new Exception("IsSeLogerDir -> sourceFolder must exist");
            }

            FileInfo[] files = sourceFolder.GetFiles(mandatoryFileName);

            if (files.Length == 1)
            {
                if (withMultipleLine)
                {
                    int lines = File.ReadLines(files[0].FullName).Count();
                    return (lines > 1);
                }
                else
                {
                    return true;
                }
            }

            return false;
        }

        public static bool IsSeLogerDir(DirectoryInfo folder, string mandatoryFileName, bool withMultipleLine = false)
        {
            if (string.IsNullOrEmpty(mandatoryFileName))
            {
                throw new Exception("Setting -> MandatoryFileName can't be null or empty");
            }

            if (!folder.Exists)
            {
                return false;
            }

            FileInfo[] files = folder.GetFiles(mandatoryFileName);

            if (files.Length == 1)
            {
                if (withMultipleLine)
                {
                    int lines = File.ReadLines(files[0].FullName).Count();
                    return (lines > 1);
                }
                else
                {
                    return true;
                }
            }

            return false;
        }

    }
}
