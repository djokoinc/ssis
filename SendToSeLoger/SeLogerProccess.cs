﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SendToSeLoger
{
    public class SeLogerProccess
    {
        public List<string> Errors { get; private set; }

        public State currentState { get; set; }

        public SeLogerFolder seLogerFolder { get; set; }

        public string proccessTmpPath { get; set; }

        public string zipPath { get; set; }

        public string zipFileName { get; set; }

        public string archivePath { get; set; }

        public enum State
        {
            Init,
            Construct,
            Archive,
            Send
        }

        public SeLogerProccess(string folderName)
        {
            seLogerFolder = new SeLogerFolder(folderName);
            Errors = new List<string>();
            currentState = State.Init;
        }

        public bool Proccessing(SeLogerContext context)
        {
            try
            {
                if (context.verbose)
                {
                    Log.setOutMethod(Log.Out.Console);
                } else
                {
                    Log.setOutMethod(Log.Out.File);
                }

                Log.Trace(string.Format("===========> Procccessing SendToLoger <==========="
                    + "{0}Data directory: {1}"
                    + "{0}Agency directory: {2}"
                    + "{0}Multi sending:{3}", 
                    Environment.NewLine,
                    context.dataFolderPath,
                    seLogerFolder.sourceFolder.Name,
                    !context.uniqueProcess));

                zipPath = CreateZipFolder(context);
                DirectoryInfo zip = new DirectoryInfo(zipPath);
                zipFileName = zip.Name;
                Log.Trace("=====> zip created");
                archivePath = ArchiveFolder(context);
                Log.Trace("=====> archive created");
                bool isSent = SendFolder(context);
                if (isSent)
                {
                    Log.Trace("=====> zip send to SeLoger.com");
                }
                
                return isSent;
            }
            catch(Exception e)
            {
                Errors.Add(e.Message);
                return false;
            }
        }

        private string CreateZipFolder(SeLogerContext context)
        {
            if (context.tmpFolderPath == null || !Directory.Exists(context.tmpFolderPath))
            {
                throw new Exception(ErrorMesssage.contextError(context));
            }

            proccessTmpPath = CreateProccessTmpFolder(context);

            // Copy folder in tmpArchive
            DirectoryInfo tmpSeLogerFolderInfo = seLogerFolder.CopyTo(proccessTmpPath);

            if (tmpSeLogerFolderInfo == null)
            {
                throw new Exception(
                    ErrorMesssage.copyFailed(
                        seLogerFolder.sourceFolder.FullName,
                        context.tmpFolderPath));
            }            

            // add files in folder
            SeLogerFolder tmpSeLogerFolder = new SeLogerFolder(tmpSeLogerFolderInfo.FullName);
            if (!tmpSeLogerFolder.AddFiles(context))
            {
                throw new Exception(
                    ErrorMesssage.addFileError(seLogerFolder.sourceFolder.FullName));
            }           

            // compress Folder
            string zipSeLogerFolderPath = tmpSeLogerFolder.getCompressFolder(context);

            currentState = State.Construct;
            return zipSeLogerFolderPath;
        }

        private string CreateProccessTmpFolder(SeLogerContext context)
        {
            // Init archiveFolder
            string dateNow = string.Format("{0}-{1}-{2}",
                DateTime.Now.Day,
                DateTime.Now.Month,
                DateTime.Now.Year);

            string tmpArchivePath = context.tmpFolderPath + @"\" + seLogerFolder.sourceFolder.Name + "_" + dateNow;

            // Create Temp directory
            if (Directory.Exists(tmpArchivePath))
            {
                DirectoryInfo tmpDir = new DirectoryInfo(tmpArchivePath);
                foreach (FileInfo file in tmpDir.GetFiles())
                {
                    file.Delete();
                }
                foreach (DirectoryInfo dir in tmpDir.GetDirectories())
                {
                    dir.Delete(true);
                }
            } else
            {
                Directory.CreateDirectory(tmpArchivePath);
            }
            
            if (!Directory.Exists(tmpArchivePath))
            {
                throw new Exception(
                    ErrorMesssage.createDirError(tmpArchivePath));
            }
            return tmpArchivePath;
        }

        private string ArchiveFolder(SeLogerContext context)
        {
            if ( (proccessTmpPath == null || !Directory.Exists(proccessTmpPath)) 
                || (context.archiveFolderPath == null || !Directory.Exists(context.archiveFolderPath)))
            {
                throw new Exception(
                    ErrorMesssage.archiveError( proccessTmpPath,
                        context.archiveFolderPath, 
                        seLogerFolder.sourceFolder.FullName));
            }

            DirectoryInfo tmpFolderInfo = new DirectoryInfo(proccessTmpPath);

            string archivePath = (context.archiveFolderPath.EndsWith(@"\") ? context.archiveFolderPath : context.archiveFolderPath + @"\") 
                + tmpFolderInfo.Name;

            Directory.CreateDirectory(archivePath);
            SeLogerFolder.CopyFolderContents(proccessTmpPath, archivePath);

            if (!Directory.Exists(archivePath))
            {
                throw new Exception(
                    ErrorMesssage.archiveError(proccessTmpPath,
                        context.archiveFolderPath,
                        seLogerFolder.sourceFolder.FullName));
            }

            currentState = State.Archive;
            return archivePath;
        }

        public bool SendFolder(SeLogerContext context)
        {
            if (!File.Exists(zipPath) || string.IsNullOrWhiteSpace(zipFileName))
            {
                Errors.Add(string.Format("SendFolder - no zip file '{0}'",
                    zipPath));
                return false;
            }

            // Get ftp request with credential
            FtpWebRequest uploadRequest = context.GetFtpUpLoadRequest(zipFileName);

            // Copy the contents of the file to the request stream.  
            using (StreamReader sourceStream = new StreamReader(zipPath))
            using (Stream requestStream = uploadRequest.GetRequestStream())
            {
                byte[] fileContents = Encoding.UTF8.GetBytes(sourceStream.ReadToEnd());
                uploadRequest.ContentLength = fileContents.Length;
                requestStream.Write(fileContents, 0, fileContents.Length);
            };

            FtpWebResponse response = (FtpWebResponse)uploadRequest.GetResponse();

            Log.Trace(string.Format("=====> Upload File Complete, status {0}", response.StatusDescription));

            response.Close();

            currentState = State.Send;
            return true;
        }
        
        internal static class ErrorMesssage
        {
            /// <summary>
            /// Copy of folder failed
            /// </summary>
            internal const string COPY_FAILED = "Directory {0} Couldn't be copy in {1}";
            internal static string copyFailed(string folderPath, string destinationPath)
            {
                return string.Format(COPY_FAILED, folderPath, destinationPath);
            }

            /// <summary>
            /// Show error on context
            /// </summary>
            internal const string CONTEXT_ERRORS = "Error on context : {0}";
            internal static string contextError(SeLogerContext context)
            {
                return string.Format(CONTEXT_ERRORS, context.ToString());
            }

            /// <summary>
            /// Error when adding files to folder
            /// </summary>
            internal const string ADD_FILES_ERRORS = "Error when adding files to directory : {0}";
            internal static string addFileError(string folderPath)
            {
                return string.Format(ADD_FILES_ERRORS, folderPath);
            }

            /// <summary>
            /// Error on Directory creation
            /// </summary>
            internal const string CREATE_DIR = "Error when creating directory : {0}";
            internal static string createDirError(string folderPath)
            {
                return string.Format(CREATE_DIR, folderPath);
            }

            /// <summary>
            /// Error on Archive creation
            /// </summary>
            internal const string ARCHIVE = "Error when creating archive directory :"
                + "{0}Archive Temp: {1}"
                + "{0}Archive : {2}"
                + "{0}source : {3}";
            internal static string archiveError(string tmpArchive, string archive, string folderPath)
            {
                return string.Format(ARCHIVE, 
                    tmpArchive,
                    archive,
                    folderPath);
            }

            /// <summary>
            /// Error on Archive creation
            /// </summary>
            internal const string SEND_DIRECTORY = "Error when sending zip directory to SeLoger.{0}"
                + "-Directory Seloger - path : {1}.{0}"
                + "{2}";
            internal static string sendError(string folderPath, string errorMessage = "")
            {
                string errorMes = (string.IsNullOrWhiteSpace(errorMessage)) ? errorMessage : string.Format("-Error Message: {0}", errorMessage);

                return string.Format(SEND_DIRECTORY,
                    Environment.NewLine,
                    folderPath,
                    errorMes);
            }

        }
    }
}
