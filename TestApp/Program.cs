﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlsApi.Controller;
using AlsApi.Model;

namespace TestApp
{
    class Program
    {
        static void Main(string[] args)
        {


            ApiCallContext apiALS = new ApiCallContext("", "", "");
            if (apiALS.tokenJWT == null)
            {
                throw new Exception("No token retrieve from ALS");
            }


            DemandeCriteria criteria = new DemandeCriteria();
            // mandatory value
            criteria.actif = true;
            criteria.emailPresent = true;
            criteria.typologies = new List<string>() { criteria.getTypoCorrespondence("T2") };
            criteria.codePostal = "75";
            criteria.ressourcesMensuellesMin = 1400 * 3;

            Console.WriteLine(criteria.ToJSONString());
            Console.ReadKey();
        }
    }
}
